!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to load AO -> MO coefficients from GAMESS
! only uncontracted and up to f functions
!
! also only cartesian representation
!
!==========================================================
subroutine loadgamessmolorbs(inmopun, innprim, indebug)

    use orbital_variables
    use common_variables

    implicit none

    ! input parameters
    integer                             :: innprim
    logical                             :: indebug
    character(len=150)                   :: inmopun

    integer                             :: iprim, jprim

    real*8                              :: CMO

    integer                             :: i

    allocate(MO(innprim,innprim))

    MO = 0.0d0

    open(unit=11, file=inmopun)

    do i=1, innprim**2

        read(11, '(2I4, E24.16)') iprim, jprim, CMO
        MO(jprim, iprim) = CMO

    end do

    close(unit=11)


    print*, "Loading AO -> MO transformation matrix of dimension", innprim, "x", innprim
    print*, "================================================================"
    print*, ""

    if (indebug .eqv. .true.) then
    
        print '(4E24.16)', MO
        print*, ""

    end if

end subroutine

subroutine loadgamessrefmolorbs(inrefmopun, inrefnnprim, indebug)

    use orbital_variables
    use common_variables

    implicit none

    ! input parameters
    integer                             :: inrefnnprim
    logical                             :: indebug
    character(len=150)                   :: inrefmopun

    integer                             :: iprim, jprim

    real*8                              :: CMO

    integer                             :: i

    allocate(refMO(inrefnnprim,inrefnnprim))

    open(unit=16, file=inrefmopun)

    do i=1, inrefnnprim**2

        read(16, '(2I4, E24.16)') iprim, jprim, CMO
        refMO(jprim, iprim) = CMO

    end do

    close(unit=16)

    print*, "Loading reference AO -> MO transformation matrix of dimension", inrefnnprim, "x", inrefnnprim
    print*, "================================================================"
    print*, ""

    if (indebug .eqv. .true.) then
    
        print '(4E24.16)', refMO

        print*, ""

    end if

end subroutine

subroutine loadrefmolorbs(inrefmopun, inrefnnprim, indebug)

    use orbital_variables
    use common_variables

    implicit none

    ! input parameters
    integer                             :: inrefnnprim
    logical                             :: indebug
    character(len=150)                   :: inrefmopun

    integer                             :: i

    allocate(refMO(inrefnnprim,inrefnnprim))

    open(unit=16, file=inrefmopun)

    ! read first line
    read(16,*)

    do i=1, inrefnnprim
        read(16, '(4F18.14)') refMO(:,i)
    end do

    close(unit=16)

    print*, "Loading reference AO -> MO transformation matrix of dimension", inrefnnprim, "x", inrefnnprim
    print*, "================================================================"
    print*, ""

    if (indebug .eqv. .true.) then
        print '(4F18.14)', refMO
    end if


end subroutine
