subroutine readgamessoverlap(inoverlapfilename, innprim, indebug)

    use orbital_variables

    implicit none

    ! input parameters
    character(len=150)                   :: inoverlapfilename
    integer                             :: innprim
    logical                             :: indebug

    integer                             :: i
    integer                             :: nlines

    integer                             :: iprim, jprim
    real*8                              :: overlap_gamess

    allocate(overlapmatrix(innprim, innprim))

    ! initialize (fill with zeros)
    overlapmatrix = 0.0d0

    nlines = 0

    open(unit=12, file=inoverlapfilename)

    ! going old-school 
    do

        read(12, *, end=10)
        nlines = nlines + 1

    end do

10 close(unit=12)

    ! open again!
    open(unit=12, file=inoverlapfilename)

    ! populate overlap matrix
    do i=1, nlines

        read(12, '(2I4,E24.16)') iprim, jprim, overlap_gamess
        overlapmatrix(iprim, jprim) = overlap_gamess
        
        ! it's symmetric
        overlapmatrix(jprim, iprim) = overlap_gamess

    end do

    close(unit=12)

    print*, "Loading overlap matrix from gamess"
    print*, "================================================================"
    print*, ""


    if (indebug .eqv. .true.) then

        do i=1, innprim

            print '(4E24.16)', overlapmatrix(:,i)

        end do

    end if

        
    print*, "================================================================"


end subroutine

