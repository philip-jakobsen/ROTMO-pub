!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
! IO units 5 = stdin (parseinp.f90)
! 6 = stdout

! 1x = DALTON
!
! 10 = dalton basis file (loadbasis.f90)

! 14 = densities from evaldens
! 15 = densities from evalrefdens
! 16 = densities from evalUdens2
! 17 = densities from evalaodens
! 18 = densities from evalmodens
! 19 = densities from evalrefaodens
! 
program main

    use common_variables

    use grid_variables

    use orbital_variables

    ! for various omp stuff
    use omp_lib

    implicit none

    integer :: i, doublefactfunc

    logical :: debug, test, printflag, evald, evalrefd, orthonorm_check

    real*8 :: pi

    character(8)  :: date
    character(10) :: time
    character(5)  :: zone
    integer,dimension(8) :: date_values

    real*8 :: main_time_start, main_time_finish

    !real*8, dimension(:), allocatable :: alphas

    call date_and_time(date,time,zone,date_values)

    ! misc values
    pi = 4*atan(1.0d0)

    print*, ""
    print*, "================================================================"
    print*, ""
    print*, "ROTMO STARTING"
    print*, ""
    !print "(A1,I4,A1,I2,A1,I2,A1,I2,A1,I2)", " ", date_values(1), "-", date_values(2), "-", &
        !& date_values(3), " ", date_values(5), ":", date_values(6)
    print "(x,a,2x,a)", date, time
    print*, ""
    print "(A20,I3,A20)", "Running on ", omp_get_num_procs(), "openMP threads"
    print*, ""
    print*, "================================================================"
    print*, ""

    printflag = .false.

    print*, "Starting cpu timing"
    call cpu_time(main_time_start)
    print*, ""
    print*, ""

    writeflag = .true.


    ! read input file from stdin
    call parse

    if (frj_interface .eqv. .true.) writeflag = .false.

    ! constructing grid writes x y z to fort.7
    call collectgrid(writeflag)

    !basisdir = "/home/philip/speciale/dalton_basis/"

    debug = .true.

    test = .true.


    evald = .true.

    orthonorm_check = .true.
    
    ! read basis from dalton basis set file
    ! also assigning cartesian coordinates to each AO
    ! arguments: <filedirectory>, <filename>, <print>
    ! output:       "exponent list" allprim 
    !               "geometry matrix" allcoords, 
    !               "angular momentum matrix" allatoml2
    call readallbasis(basisdir, basisfilename, inp_print)

    ! read reference basis from dalton basis set file
    ! also assigning cartesian coordinates to each AO
    !
    ! arguments:    <filedirectory>, <filename>, <print>
    ! output:       "exponent list" refallprim 
    !               "geometry matrix" refallcoords, 
    !               "angular momentum matrix" refallatoml2
    call readallrefbasis(basisdir, refbasisfilename, inp_print)

    ! making double factorials
    do i=1,30

        ! offset of 2, lowest value is -1
        doublefact(i) = doublefactfunc(i-2)

    end do

    ! read overlap matrix from gamess
    ! arguments: <filename>, <dimension>, <print>
    ! output:       "overlap matrix" overlapmatrix
    call readgamessoverlap(overlapfilename, wnnprim, inp_print)

    ! load AO -> MO coefficients from file
    ! last argument is print MOs
    ! arguments: <filename>, <dimension> <print>
    ! output:       "C matrix" MO
    call loadgamessmolorbs(mopun, wnnprim, inp_print)

    ! load reference AO -> MO coefficients 
    ! arguments: <filename>, <dimension> <print>
    ! output:       "reference C matrix" refMO
    call loadgamessrefmolorbs(refmopun, refnnprim, inp_print)

    ! load 1-electron density matrix in AO basis 
    ! arguments: <filename>, <dimension> <print>
    ! output:       "density matrix in AO basis" gamessAOdensitymatrix
    !call loadgamessAOD(densitymatrixfilename, wnnprim, inp_print)

    ! load reference 1-electron density matrix in AO basis 
    ! arguments: <filename>, <dimension> <print>
    ! output:       "reference density matrix in AO basis" gamessCIAOdensitymatrix
    call loadgamessCIAOD(refdensitymatrixfilename, refnnprim, inp_print)

    ! load symmetry labels, assuming file "SYM" in same directory as HF CMO etc
    ! arguments: <filename>, <print>
    ! output:       "list of symmetry labels for each MO" symlabels
    call loadsym(symfilename, inp_print)

    ! manually make 1-electron density matrix in MO basis
    ! obviously only works for restricted HF systems
    print*, "Manually making density matrix in MO basis (only RHF)"
    allocate(densitymatrix(wnnprim,wnnprim))
    densitymatrix = 0.0d0
    do i=1, nocc 
        densitymatrix(i,i) = 2.0d0
    end do

    ! 
    ! the following is just a check of the AO-MO transformation 
    !
    !! temporarily disabling this when testing NOs
    !print*, "Checking if D matrices in HF are correct..."
    !print*, "Condition: (C.D(MO)).C^T = D(AO) "

    !if (sum(matmul(matmul(MO,densitymatrix),transpose(MO))-gamessAOdensitymatrix) < 1.0d-12) then

        !print*, "Deviation = ", sum(matmul(matmul(MO,densitymatrix),transpose(MO))-gamessAOdensitymatrix)
        !print*, "Density matrices for HF looks good to me"

    !else

        !print*, "Deviations = ", sum(matmul(matmul(MO,densitymatrix),transpose(MO))-gamessAOdensitymatrix)
        !error stop "Something is wrong, D(AO) -> D(MO) transformation does not result in the generated D(MO)"

    !end if

    !print*, "Evaluating HF densities on grid in AO basis"
    !! evaluate densities on grid in AO basis
    !! arguments: <threshold for element D(i,j)>, <write>
    !! output:      "density on grid" aodens, if write also x,y,z,rho,w to file fort.17
    !call evalaodens(dtol, writeflag)

    !! check number of electrons
    !! arguments: <density on grid>, <printflag>
    !call sumdens(aodens, .true.)
    !print*, ""
    !print*, ""
    !print*, ""

    print*, "Evaluating HF densities on grid in MO basis"
    ! evaluate densities on grid in MO basis
    ! arguments: <thresh for D(i,j) element> <write> 
    ! outout:      "density on grid" dens, if write also x,y,z,rho,w to file fort.14
    call evaldens(dtol, writeflag)

    ! check number of electrons
    call sumdens(dens, .true.)
    print*, ""
    print*, ""
    print*, ""
    print "(A30, E12.6)", "Accuracy of integration = ", abs(dble(sum(atnums)) - nelectrons)

    ! check orthonormality of MOs
    if (orthonorm_check .eqv. .true.) then

        print*, "================================================================"
        print*, "Checking orthonormality of MOs"

        ! arguments: <type>, <print>, <tolerance>
        ! <type> is either "MO " or "UMO"
        call ortho_check_all("MO ", .false., orthotol)

        print "(A32, E12.6, A10, 2I3)", "Largest deviation of overlap = ", maxval(orthomat), "of pairs ", maxloc(orthomat)

    end if

    evalrefd = .true.

    ! generate reference densities on grid
    ! and write to fort.15
    if (evalrefd .eqv. .true.) then

        call restart_refdens()

    end if
        
    allocate(converged_alphas(nocc,wnnprim))
    allocate(converged_alphas_previous(nocc,wnnprim))
    converged_alphas = 0.0d0


    ! load restarted alphas from fort.90
    if (RESTART .eqv. .TRUE.) then

        call restart_alphas()

    end if

    !allocate(hist_alphas(nocc,wnnprim,maxsweepiter))
    !hist_alphas = 0.0d0

    if (frj_interface .eqv. .true.) then

        if (allocated(UMO) .eqv. .false.) then
            
            allocate(UMO(wnnprim,wnnprim))

            UMO = MO

        end if

        call opt_interface(.true.)

        print*, "Interface calculation complete"
        
        call date_and_time(date,time,zone,date_values)
        
        print "(2A20)", date, time

        return

    end if

    call cpu_time(main_time_finish)


    print*, ""
    print*, "================================================================"
    print*, ""
    print*, "ROTMO ENDED GRACEFULLY"
    print*, ""
    print "(2A20)", date, time
    print*, ""
    print*, "================================================================"
    print*, ""


end program main

