!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutines to calculate analytical gradient vector
!
subroutine calc_grad_vec(inalphas, UMO_rotated, outvec)

    !use sweep_variables
    use orbital_variables

    implicit none

    real*8, dimension(nangles) :: inalphas
    real*8, dimension(nangles) :: outvec
    real*8, dimension(wnnprim,wnnprim) :: UMO_rotated
    integer :: iocc,ivir,nvir,kj_var

    nvir = wnnprim - nocc

    ! construct gradient vector, looping over all combinations of occupied and virtuals
    do iocc=1,nocc
      do ivir=nocc+1, wnnprim
        if (symcheck .eqv. .true.) then
            if (symlabels(iocc) .ne. symlabels(ivir)) then
                !print "(A20, 2I4, A20)", "Skipping pair", iocc, ivir, "due to symmetry"
                cycle
            end if
        end if
        call calc_grad5(inalphas, UMO_rotated, iocc, ivir)
        kj_var = (iocc-1)*nvir + (ivir-nocc)
        outvec(kj_var) = dFdT
      end do
    end do

end subroutine calc_grad_vec

! subroutines to calculate analytical hessian matrix
subroutine calc_hess_mat(inalphas, UMO_rotated, outmat)

    use orbital_variables
    !use sweep_variables
    use grid_variables
    use common_variables

    implicit none

    real*8, dimension(nangles) :: inalphas
    real*8, dimension(nangles,nangles) :: outmat
    real*8, dimension(wnnprim,wnnprim) :: UMO_rotated
    integer :: ivir, jvir
    integer :: iocc, jocc
    integer :: nvir, kj_var,mn_var
    integer :: i

    outmat = 0.0d0

    ! initialize with large identity, to level-shift away the frozen variables
    do i=1, nangles
        outmat(i,i) = 1.0d+01
    end do

    nvir = wnnprim - nocc

    ! loop over all double pairs of occupied and virtuals
    do iocc=1,nocc
      do ivir=nocc+1,wnnprim
        if (symcheck .eqv. .true.) then
           if ( symlabels(iocc) .ne. symlabels(ivir) ) then
              cycle
           end if
        end if
        kj_var = (iocc-1)*nvir + (ivir-nocc)
        do jocc=1,nocc
          do jvir=nocc+1,wnnprim
            if (symcheck .eqv. .true.) then
               if (symlabels(jocc) .ne. symlabels(jvir) ) then
                  cycle
               end if
            end if
            call full_hess6(inalphas,UMO_rotated,iocc,ivir,jocc,jvir)
            mn_var = (jocc-1)*nvir + (jvir-nocc)
            outmat(kj_var,mn_var) = hij
          end do
        end do
      end do
    end do

end subroutine calc_hess_mat

