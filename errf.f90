!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to calculate ErrF, normalized by number of electrons
subroutine calcerrf(indens, inrefdens, inprint)

    use common_variables

    use grid_variables

    implicit none

    ! input parameters
    real*8, dimension(npoints)          :: inrefdens
    real*8, dimension(npoints)          :: indens

    logical                             :: inprint

    ErrF_value = 0.0d0

    ! ErrFNorm as the normalization to make ErrF grid independent
    ErrF_Norm = sum((inrefdens*grid(4,:))**2)
    ErrF_value = sum((inrefdens*grid(4,:)-indens*grid(4,:))**2) / ErrF_Norm

    print '(A10, E24.16)', "ErrF = ", ErrF_value
    if (inprint .eqv. .true. ) then

        print*, "================================================================"
        print '(A10, E24.16)', "ErrF = ", ErrF_value
        print*, ""

    end if

end subroutine

! subroutine to calculate Vne and Jee from given density and nuclei
subroutine calcVneJee(indens, inrefdens)

    use common_variables

    use grid_variables

    implicit none

    ! input parameters
    real*8, dimension(npoints)          :: inrefdens
    real*8, dimension(npoints)          :: indens

    logical                             :: doJee

    integer                             :: i,j
    real*8                              :: xgi,ygi,zgi,wgi
    real*8                              :: xgj,ygj,zgj,wgj
    real*8                              :: dis, pot, eps
    real*8                              :: VneREF, VneFIT
    real*8                              :: JeeREF, JeeFIT

    !FRJ: optional turn on Jee calculation, hardwired at present
    !FRJ: also change doderivs in opt_interface.f90 to avoid calculating gradient 
    !     and hessian when used for Vne and Jee calculation at final point
    doJee = .false.
    !doJee = .true.

    VneREF = 0.0d0
    VneFIT = 0.0d0

    !$omp parallel do private(xgi, ygi, zgi, wgi, dis, pot) &
    !$omp & reduction(+:VneREF,VneFIT)

    do i = 1,npoints
      xgi = grid(1, i)
      ygi = grid(2, i)
      zgi = grid(3, i)
      wgi = grid(4, i)
      pot = 0.0d0
      do j=1,natoms
        dis = sqrt((xgi-coords(1,j))**2+(ygi-coords(2,j))**2+(zgi-coords(3,j))**2)
        pot = pot - atnums(j)/dis
      enddo
      VneREF = VneREF + inrefdens(i)*pot*wgi
      VneFIT = VneFIT + indens(i)*pot*wgi
    enddo

    !$omp end parallel do

    print '(A15, E24.16)', "Vne REF       =", VneREF
    print '(A15, E24.16)', "Vne FIT       =", VneFIT
    print '(A15, E24.16)', "Vne REF - FIT =", VneREF - VneFIT
    print '(A15, E24.16)', "Vne rel-error =", (VneREF - VneFIT)/VneREF

    ! Coulomb part of Vee
    ! this could be combined with the Vne calculation for the outer grid loop
    ! It is Npoint quadratic and thus optional invoked by doJee
    ! The results have been checked against G16 Jee (link 608), and are valid, but a
    ! large angular grid is required to get a decent agreement
    if (doJee) then
      JeeREF = 0.0d0
      JeeFIT = 0.0d0
      eps    = 1.0d-14

      !$omp parallel do private(xgi, ygi, zgi, wgi, xgj, ygj, zgj, wgj, dis, eps) &
      !$omp & reduction(+:JeeREF,JeeFIT)

      do i = 1,npoints
        xgi = grid(1, i)
        ygi = grid(2, i)
        zgi = grid(3, i)
        wgi = grid(4, i)
        !do j=1,npoints
        do j=i+1,npoints
          xgj = grid(1, j)
          ygj = grid(2, j)
          zgj = grid(3, j)
          wgj = grid(4, j)
          dis = sqrt((xgi-xgj)**2+(ygi-ygj)**2+(zgi-zgj)**2)
          if (dis > eps) then
            JeeREF = JeeREF + inrefdens(i)*inrefdens(j)*wgi*wgj/dis
            JeeFIT = JeeFIT + indens(i)*indens(j)*wgi*wgj/dis
          endif
        enddo
      enddo

      !$omp end parallel do

      print '(A15, E24.16)', "Jee REF       =", JeeREF
      print '(A15, E24.16)', "Jee FIT       =", JeeFIT
      print '(A15, E24.16)', "Jee REF-FIT   =", JeeREF - JeeFIT
      print '(A15, E24.16)', "Jee rel-error =", (JeeREF - JeeFIT)/JeeREF
    endif

end subroutine

