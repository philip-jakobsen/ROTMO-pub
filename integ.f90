!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to create integration grid, adapted from mrcc
! 
! angular grid is of the lebedev type and radial grid is
! log3 from mura and knowles
!
!==========================================================
subroutine makegrid(innrgrid, innagrid, inlebedev_x, inlebedev_y, inlebedev_z, inlebedev_w, innatoms, inatnums, incoords)

    use common_variables

    use grid_variables

    implicit none

    ! input parameters
    integer                                 :: innagrid, innrgrid
    real*8, dimension(innagrid)             :: inlebedev_x, inlebedev_y, inlebedev_z, inlebedev_w
    integer                                 :: innatoms
    integer, dimension(innatoms)            :: inatnums
    real*8, dimension(3,innatoms)           :: incoords(3,innatoms)


    integer                                 :: ngrid, nragrid
    integer                                 :: iagrid, irgrid, iragrid

    real*8                                  :: x, rgrid, rweight
    real*8                                  :: xcoord, ycoord, zcoord
    real*8                                  :: xgr, ygr, zgr, wgr
    real*8                                  :: grtol

    real*8, dimension(119)                  :: bragg
    integer, dimension(24)                  :: alp5
    integer, dimension(6)                   :: alp7

    real*8, dimension(innatoms,innatoms)    :: distc
    integer                                 :: atcl(innatoms)

    real*8, dimension(innatoms)             :: ragr, p
    real*8                                  :: muij, rri, rij, chi, aij, uij, pj, ri, rj, s, psum, sum

    integer                                 :: iatoms, jatoms, katoms, kkatoms, alp, iatnum


    grtol = 10.0**(-10.d0)

    ! bragg-slater atomic radii, taken from mrcc @ integ.f, subroutine dftgrid
    ! used in the becke fuzzy integration
    bragg = (/ & 
            & 70.d0, 35.d0,32.d0,145.d0,105.d0,85.d0,70.d0,65.d0,60.d0,       &
            & 50.d0,69.d0,180.d0,150.d0,125.d0,110.d0,100.d0,100.d0,100.d0,   &
            & 71.d0,220.d0,180.d0,160.d0,140.d0,135.d0,140.d0,140.d0,140.d0,  &
            & 135.d0,135.d0,135.d0,135.d0,130.d0,125.d0,115.d0,115.d0,115.d0, & 
            & 110.d0,235.d0,200.d0,180.d0,155.d0,145.d0,145.d0,135.d0,130.d0, & 
            & 135.d0,140.d0,160.d0,155.d0,155.d0,145.d0,145.d0,140.d0,140.d0, & 
            & 130.d0,260.d0,215.d0,195.d0,185.d0,185.d0,185.d0,185.d0,185.d0, & 
            & 185.d0,180.d0,175.d0,175.d0,175.d0,175.d0,175.d0,175.d0,175.d0, & 
            & 155.d0,145.d0,135.d0,135.d0,130.d0,135.d0,135.d0,135.d0,150.d0, & 
            & 190.d0,180.d0,160.d0,190.d0,150.d0,120.d0,260.d0,215.d0,195.d0, & 
            & 180.d0,180.d0,175.d0,175.d0,175.d0,175.d0,169.d0,168.d0,168.d0, & 
            & 165.d0,167.d0,173.d0,176.d0,161.d0,157.d0,149.d0,143.d0,141.d0, & 
            & 134.d0,129.d0,128.d0,121.d0,122.d0,136.d0,143.d0,162.d0,175.d0, & 
            & 165.d0,157.d0 /)

    ! atoms where alp=5, https://aip.scitation.org/doi/pdf/10.1063/1.471749
    alp5 = (/ & 
            & 1,2,                        & ! first row, H-He
            & 5,6,7,8,9,10,                 & ! second row, B-Ne
            & 13,14,15,16,17,18,            & ! third row, Al-Ar
            & 21,22,23,24,25,26,27,28,29,30 & ! d block, Sc-Zn
            & /)

    ! atoms where alp=7, table IV in https://aip.scitation.org/doi/pdf/10.1063/1.471749
    alp7 = (/ & 
            & 3,4, & ! Li-Be
            & 11,12, & ! Na-Mg
            & 19,29 & ! K-Ca
            & /)

    npoints = innrgrid*innagrid*innatoms

    allocate(grid(4,npoints))         

    ! calculate interatomic distances
    do iatoms=1, innatoms
        distc(iatoms, iatoms) = 0.0d0
        do jatoms=iatoms+1, innatoms
            distc(iatoms, jatoms) = sqrt((incoords(1,iatoms)-incoords(1,jatoms))**2+ &
                                       & (incoords(2,iatoms)-incoords(2,jatoms))**2+ &
                                       & (incoords(3,iatoms)-incoords(3,jatoms))**2)
            ! matrix is symmetric of course
            distc(jatoms, iatoms) = distc(iatoms, jatoms)
        end do
    end do

    alp = 5.0d0

    nragrid = 0
    ngrid = 0

    ! start natoms loop
    do iatoms=1, innatoms
        
        nragrid = ngrid

        iatnum = inatnums(iatoms)

        ! assign correct value of alp
        ! by checking if atom belongs to
        ! either alp5 or alp7 list
        if (any (alp5 .eq. iatnum)) then
            alp = 5.0d0
        else if (any (alp7 .eq. iatnum)) then
            alp = 7.0d0
        else 
            print*, "Atom Z = ", iatnum, " is not supported"
        end if

        xcoord = incoords(1, iatoms)
        ycoord = incoords(2, iatoms)
        zcoord = incoords(3, iatoms)

!omp$ parallel do

        ! radial loop
        do irgrid=1, innrgrid

            ! ========== Log3 from Mura and Knowles
            x = dble(irgrid)/dble(innrgrid+1)
            rgrid = -alp*log(1.d0-x**3)
            rweight = 3.d0*rgrid**2*alp*x**2/((1.d0-x**3)*dble(innrgrid+1))

            ! angular loop
            do iagrid=1, innagrid
                
                nragrid = nragrid + 1

                grid(1, nragrid) = rgrid*inlebedev_x(iagrid)+xcoord
                grid(2, nragrid) = rgrid*inlebedev_y(iagrid)+ycoord
                grid(3, nragrid) = rgrid*inlebedev_z(iagrid)+zcoord
                grid(4, nragrid) = inlebedev_w(iagrid)*rweight

            end do !iagrid
        end do !radial loop

!omp$ end parallel do

        ! sort atoms for some reason
        call qsortd(atcl, innatoms, distc(1, iatoms))

        do iragrid = ngrid + 1, nragrid

            ! assign x y z and weight to grid
            xgr = grid(1, iragrid)
            ygr = grid(2, iragrid)
            zgr = grid(3, iragrid)
            wgr = grid(4, iragrid)

            ! find distances
            do jatoms=1, innatoms
                ragr(jatoms) = sqrt((xgr-incoords(1,jatoms))**2+(ygr-incoords(2,jatoms))**2+(zgr-incoords(3,jatoms))**2)
            end do

            psum = 0.0d0

            do jatoms=1, innatoms

                pj = 1.0d0

                rri = bragg(inatnums(jatoms))
        
                ri = ragr(jatoms)

                do kkatoms = 1, innatoms

                    katoms = atcl(kkatoms)

                    if (katoms .eq. jatoms) cycle

                    ! distance between the two atoms
                    rij = distc(katoms, jatoms)

                    chi = rri/bragg(inatnums(katoms))

                    rj = ragr(katoms)

                    uij = (chi - 1.0d0)/(chi + 1.0d0)
                    aij = min(0.5d0, max(-0.5d0, uij/(uij**2-1.0d0)))
                    muij = (ri - rj) / rij

                    s = muij+aij*(1.0d0-muij**2)

                    ! Murray, m_mu = 10
                    muij=1.d0-s*s                            

                    sum=-0.5d0*s                             
                    s=0.5d0+sum                              
                    sum=sum*muij*0.5d0                       
                    s=s+sum                                  
                    sum=sum*muij*0.75d0                      
                    s=s+sum                                  
                    sum=sum*muij*0.83333333333333333333d0    
                    s=s+sum                                  
                    sum=sum*muij*0.875d0                     
                    s=s+sum                                  
                    sum=sum*muij*0.9d0                       
                    s=s+sum                                  
                    sum=sum*muij*0.91666666666666666666d0    
                    s=s+sum                                  
                    sum=sum*muij*0.92857142857142857142d0    
                    s=s+sum                                  
                    sum=sum*muij*0.9375d0                    
                    s=s+sum                                  
                    sum=sum*muij*0.94444444444444444444d0    
                    s=s+sum+sum*muij*0.95d0                  

                    pj = pj*s

                    if (pj .lt. grtol) exit

                end do ! katoms

            psum = psum+pj

            p(jatoms) = pj

            end do ! jatm

            wgr = wgr *p(iatoms)/psum

            ! screening points like mrcc
            !if (abs(wgr) .gt. 1.0d-4*grtol) then

                ! save cartesian coordinates and fuzzy weight
                ngrid = ngrid+1

                grid(1, ngrid) = xgr
                grid(2, ngrid) = ygr
                grid(3, ngrid) = zgr
                grid(4, ngrid) = wgr

                !write(8, '(5E20.10)') xgr, ygr, zgr, wgr, p(iatoms)/psum

            !end if

            !end do ! angular loop
        end do ! rad loop
    end do ! iatm loop

    end subroutine makegrid

!***********************************************************************
      SUBROUTINE QSORTD (ORD,N,A) ! from combin.f in mrcc
!***********************************************************************
! SORTS THE ARRAY A(I),I=1,2,...,N BY PUTTING THE
! ASCENDING ORDER VECTOR IN ORD.  THAT IS ASCENDING ORDERED A
! IS A(ORD(I)),I=1,2,...,N; DESCENDING ORDER A IS A(ORD(N-I+1)),
! I=1,2,...,N .  THIS SORT RUNS IN TIME PROPORTIONAL TO N LOG N .
!***********************************************************************
      IMPLICIT INTEGER (A-Z)

      DIMENSION ORD(N),POPLST(2,20)
      REAL*8 X,XX,Z,ZZ,Y

!     TO SORT DIFFERENT INPUT TYPES, CHANGE THE FOLLOWING
!     SPECIFICATION STATEMENTS; FOR EXAMPLE, FOR FORTRAN CHARACTER
!     USE THE FOLLOWING:  CHARACTER *(*) A(N)

      REAL*8  A(N)

      NDEEP=0
      U1=N
      L1=1
      DO 1  I=1,N
    1 ORD(I)=I
    2 IF (U1.LE.L1) RETURN

    3 L=L1
      U=U1

! PART

    4 P=L
      Q=U
!     FOR CHARACTER SORTS, THE FOLLOWING 3 STATEMENTS WOULD BECOME
!     X = ORD(P)
!     Z = ORD(Q)
!     IF (A(X) .LE. A(Z)) GO TO 2

!     WHERE "CLE" IS A LOGICAL FUNCTION WHICH RETURNS "TRUE" IF THE
!     FIRST ARGUMENT IS LESS THAN OR EQUAL TO THE SECOND, BASED ON "LEN"
!     CHARACTERS.

      X=A(ORD(P))
      Z=A(ORD(Q))
      IF (X.LE.Z) GO TO 5
      Y=X
      X=Z
      Z=Y
      YP=ORD(P)
      ORD(P)=ORD(Q)
      ORD(Q)=YP
    5 IF (U-L.LE.1) GO TO 15
      XX=X
      IX=P
      ZZ=Z
      IZ=Q
!
! LEFT
!
    6 P=P+1
      IF (P.GE.Q) GO TO 7
      X=A(ORD(P))
      IF (X.GE.XX) GO TO 8
      GO TO 6
    7 P=Q-1
      GO TO 13
!
! RIGHT
!
    8 Q=Q-1
      IF (Q.LE.P) GO TO 9
      Z=A(ORD(Q))
      IF (Z.LE.ZZ) GO TO 10
      GO TO 8
    9 Q=P
      P=P-1
      Z=X
      X=A(ORD(P))
!
! DIST
!
   10 IF (X.LE.Z) GO TO 11
      Y=X
      X=Z
      Z=Y
      IP=ORD(P)
      ORD(P)=ORD(Q)
      ORD(Q)=IP
   11 IF (X.LE.XX) GO TO 12
      XX=X
      IX=P
   12 IF (Z.GE.ZZ) GO TO 6
      ZZ=Z
      IZ=Q
      GO TO 6
!
! OUT
!
   13 CONTINUE
      IF (.NOT.(P.NE.IX.AND.X.NE.XX)) GO TO 14
      IP=ORD(P)
      ORD(P)=ORD(IX)
      ORD(IX)=IP
   14 CONTINUE
      IF (.NOT.(Q.NE.IZ.AND.Z.NE.ZZ)) GO TO 15
      IQ=ORD(Q)
      ORD(Q)=ORD(IZ)
      ORD(IZ)=IQ
   15 CONTINUE
      IF (U-Q.LE.P-L) GO TO 16
      L1=L
      U1=P-1
      L=Q+1
      GO TO 17
   16 U1=U
      L1=Q+1
      U=P-1
   17 CONTINUE
      IF (U1.LE.L1) GO TO 18
!
! START RECURSIVE CALL
!
      NDEEP=NDEEP+1
      POPLST(1,NDEEP)=U
      POPLST(2,NDEEP)=L
      GO TO 3
   18 IF (U.GT.L) GO TO 4
!
! POP BACK UP IN THE RECURSION LIST
!
      IF (NDEEP.EQ.0) GO TO 2
      U=POPLST(1,NDEEP)
      L=POPLST(2,NDEEP)
      NDEEP=NDEEP-1
      GO TO 18
!
! END SORT
! END QSORT
!
      END subroutine qsortd

