ar -crsv philip.a \
parse_input.o \
load_lebedev.o \
integ.o \
nel.o \
construct_grid.o \
load_cmo.o \
cartfunctions.o \
grid_variables.o \
orbital_variables.o \
common_variables.o \
load_overlap.o \
load_basis.o \
load_1eD.o \
dens.o \
ortho.o \
errf.o \
load_sym.o \
restart.o \
grad.o \
opt_interface.o \
deriv.o \
rotmo.o

