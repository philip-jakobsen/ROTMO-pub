!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!

subroutine loadgamessCIMOD(in1eD, innprim, indebug)

    use orbital_variables
    use common_variables

    implicit none

    ! input parameters
    integer                             :: innprim
    logical                             :: indebug
    character(len=150)                   :: in1eD

    integer                             :: i, iprim, jprim, nlines
    real*8                              :: delement

    nlines = 0

    print '(A80, I5, A3, I5)', "Loading CI MO basis 1-electron density matrix of dimension", innprim, "x", innprim
    print*, "================================================================"
    print*, ""

    allocate(gamessCIMOdensitymatrix(innprim,innprim))
    gamessCIMOdensitymatrix = 0.0d0

    open(unit=13, file=in1ed)

    do
        read(13, *, end=10)
        nlines = nlines + 1
    end do


10 close(unit=13)

    open(unit=13, file=in1eD)

    do i=1, nlines

            read(13, '(2I4, E24.12)') iprim, jprim, delement
            gamessCIMOdensitymatrix(iprim, jprim) = delement
            gamessCIMOdensitymatrix(jprim, iprim) = delement

    end do

    close(unit=13)

    if (indebug .eqv. .true.) then
        print '(4E24.16)', gamessCIMOdensitymatrix
    end if

    close(unit=13)

    print*, ""

end subroutine

subroutine loadgamessAOD(in1eD, innprim, indebug)

    use orbital_variables
    use common_variables

    implicit none

    ! input parameters
    integer                             :: innprim
    logical                             :: indebug
    character(len=150)                   :: in1eD

    integer                             :: i, iprim, jprim, nlines
    real*8                              :: delement

    nlines = 0

    print '(A50, I5, A3, I5)', "Loading AO basis 1-electron density matrix of dimension", innprim, "x", innprim
    print*, "================================================================"
    print*, ""

    allocate(gamessAOdensitymatrix(innprim,innprim))
    gamessAOdensitymatrix = 0.0d0

    open(unit=13, file=in1ed)

    do
        read(13, *, end=10)
        nlines = nlines + 1
    end do


10 close(unit=13)

    open(unit=13, file=in1eD)

    do i=1, nlines

            read(13, '(2I4, E24.12)') iprim, jprim, delement
            gamessAOdensitymatrix(iprim, jprim) = delement
            gamessAOdensitymatrix(jprim, iprim) = delement

    end do

    close(unit=13)

    if (indebug .eqv. .true.) then
        print '(4E24.16)', gamessAOdensitymatrix
    end if

    close(unit=13)

    print*, ""

end subroutine

subroutine loadgamessCIAOD(in1eD, innprim, indebug)

    use orbital_variables
    use common_variables

    implicit none

    ! input parameters
    integer                             :: innprim
    logical                             :: indebug
    character(len=150)                   :: in1eD

    integer                             :: i, iprim, jprim, nlines
    real*8                              :: delement

    nlines = 0

    print '(A50, I5, A3, I5)', "Loading CI AO basis 1-electron density matrix of dimension", innprim, "x", innprim
    print*, "================================================================"
    print*, ""

    allocate(gamessCIAOdensitymatrix(innprim,innprim))
    gamessCIAOdensitymatrix = 0.0d0

    open(unit=13, file=in1ed)

    do
        read(13, *, end=10)
        nlines = nlines + 1
    end do


10 close(unit=13)

    open(unit=13, file=in1eD)

    do i=1, nlines

            read(13, '(2I4, E24.16)') iprim, jprim, delement
            gamessCIAOdensitymatrix(iprim, jprim) = delement
            gamessCIAOdensitymatrix(jprim, iprim) = delement

    end do

    close(unit=13)

    if (indebug .eqv. .true.) then
        print '(4E24.16)', gamessCIAOdensitymatrix
    end if

    close(unit=13)

    print*, ""

end subroutine

