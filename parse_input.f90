!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to parse input file from stdin
! 
!==========================================================

subroutine parse

    use common_variables

    use grid_variables

    use orbital_variables

    !use sweep_variables

    !use fit_variables

    implicit none

    ! number of lines
    integer                                             :: nlines

    integer                                             :: i, iatoms

    ! length of each
    character(len=512), dimension(500)                  :: inpline

    integer                                             :: counter

    ! initalize
    counter = 0

    ! read input from stdin
    ! first line is number of lines in input file
    read(5, *) nlines

    rewind(5)

    do i=1,nlines

        read(5, '(A)') inpline(i)

    end do

    ! parse input parameters
    basisdir= trim(inpline(3))
    read(inpline(5), *) nrgrid
    read(inpline(7), '(I4)') nagrid
    read(inpline(9), *) natoms

    allocate(atnums(natoms))
    allocate(coords(3,natoms))
    allocate(basisfilename(natoms))
    allocate(refbasisfilename(natoms))
    allocate(atlabel(natoms))

    ! read atom number and xyz coordinates and basis and reference basis
    do iatoms=1, natoms
        read(inpline(10+iatoms), *) atlabel(iatoms), atnums(iatoms), coords(1,iatoms), coords(2, iatoms), coords(3, iatoms), &
                                  & basisfilename(iatoms), refbasisfilename(iatoms)
    end do

    counter = 10+natoms+2

    ! read MOPUN file
    mopun = trim(inpline(counter))
    counter = counter + 2

    ! read overlap file
    overlapfilename = trim(inpline(counter))
    counter = counter + 2

    ! read density matrix from lucita
    densitymatrixfilename = trim(inpline(counter))
    counter = counter + 2

    ! read reference (FCI) 1-electron density matrix
    refdensitymatrixfilename = trim(inpline(counter))
    counter = counter + 2

    ! read reference (FCI) MOPUN file
    refmopun = trim(inpline(counter))
    counter = counter + 2

    ! read sym labels (HF)
    symfilename = trim(inpline(counter))
    counter = counter + 2

    ! read input print
    read(inpline(counter), "(L)") inp_print
    counter = counter + 2

    ! read number of frozen orbitals
    read(inpline(counter), *) nfc
    counter = counter + 2

    ! threshold for evaluation of D(i,i) (in either AO or MO basis)
    read(inpline(counter), *) dtol
    counter = counter + 2

    read(inpline(counter), "(L)") RESTART
    counter = counter + 2

    read(inpline(counter), "(L)") frj_interface
    counter = counter + 2

    if (frj_interface .eqv. .true. ) then

        read(inpline(counter), *) nocc
        counter = counter + 2

        read(inpline(counter), *) orthotol
        counter = counter + 2

        read(inpline(counter), "(L)") symcheck
        counter = counter + 2

    end if

    npoints = nrgrid*nagrid*natoms

    ! print input parameters
    print*, "================================================================"
    print*, "input parameters from input file"
    print*, "================================================================"
    !print '(A20, A50)', "data root dir = ", trim(data_dir)
    print '(A20, A50)', "CMO file = ", trim(mopun)
    print '(A20, A50)', "overlap file = ", trim(overlapfilename)
    print '(A20, A50)', "1e density file = ", trim(densitymatrixfilename)
    print '(A20, A50)', "ref 1e density file = ", trim(refdensitymatrixfilename)
    print '(A20, A50)', "ref CMO file = ", trim(refmopun)
    print '(A30, I5)', "Number of occupied orbitals = ", nocc
    print '(A30, I5)', "Number of frozen orbitals = ", nfc
    print '(A20, I8)', "natoms = ", natoms
    print '(A20, I8)', "nrgrid = ", nrgrid
    print '(A20, I8)', "nagrid = ", nagrid
    print '(A20, I12)', "npoints = ", npoints
    print '(A20, E12.6)', "orthotol = ", orthotol
    print '(A20, L)', "input print = ", inp_print
    print '(A60)', adjustl("label atnum geometry (x, y, z)               basis    refbasis")
    
    do iatoms=1, natoms
        print '(A5, I3, 3f12.6, 4X, 2A12)', atlabel(iatoms), atnums(iatoms), coords(1,iatoms), coords(2,iatoms), coords(3,iatoms), &
                                         & basisfilename(iatoms), refbasisfilename(iatoms)
    end do


    print*, "================================================================"
    print*, ""
    print*, ""

end subroutine parse

