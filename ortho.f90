!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
! subroutine to check orthonormality of two (rotated) MOs
subroutine orthonormal_check(ini, inj, innprim, intol, inprint, intype)

    use orbital_variables

    implicit none

    character*3                 :: intype

    logical                     :: inprint

    integer                     :: ini, inj, mu, nu

    real*8                      :: intol

    integer                     :: innprim

    real*8                      :: tmpS

    tmpS = 0.0d0

    if (inprint .eqv. .true.) then

        print*, "orthonormal_check speaking"
        print*, "ini = ", ini
        print*, "inj = ", inj

    end if

    do mu=1, innprim

        do nu=1, innprim

            if (intype == "MO") then

                tmpS = tmpS + MO(mu, ini) * MO(nu, inj) * overlapmatrix(mu, nu)

            else if (intype == "UMO") then

                tmpS = tmpS + UMO(mu, ini) * UMO(nu, inj) * overlapmatrix(mu, nu)

            end if

        end do

    end do


    overlap = tmpS

    ! case i = j
    if (ini .eq. inj) then

        if (abs(overlap - 1.0d0) < intol ) then

            if (inprint .eqv. .true.) then

                print "(A20, E20.12)", "<i|j> = ", overlap
                print*, "MO ", ini, "and", inj, "are orthonormal"
                
            end if

        else

            print "(A20, E24.16)", "Overlap tol = ", intol
            print "(A20, E24.16)", "<i|j> = ", overlap
            print "(A20, E24.16)", "diff = ", abs(1.0 - overlap)
            print*, "Something is wrong, MO ",  ini, "and", inj, "are NOT orthonormal"
            print*, "exiting"

            error stop "MOs not orthonormal"

        end if

        ! for check with max deviation
        overlap = 1.0d0 - overlap

    ! case i != j
    else

        if (abs(overlap) < intol ) then

            if (inprint .eqv. .true.) then
                
                print "(A20, E20.12)", "<i|j> = ", overlap
                print*, "MO ", ini, "and", inj, "are orthonormal"

            end if

        else

            print "(A20, E20.12)", "<i|j> = ", overlap
            print*, "Something is wrong, MO ",  ini, "and", inj, "are NOT orthonormal"

            error stop "MOs not orthonormal"

        end if

    end if


end subroutine


subroutine ortho_check_all(intype, inprint, inorthotol)

    use orbital_variables

    integer                     :: i, j

    character*3                 :: intype

    logical                     :: inprint

    real*8                      :: inorthotol

    if (allocated(orthomat) .eqv. .false.) then

        allocate(orthomat(wnnprim, wnnprim))

        orthomat = 0.0d0

    end if


    if (inprint .eqv. .true.) then

        print*, "nprim = ", wnnprim

    end if


    do i=1, wnnprim
        
        do j=1, wnnprim

            call orthonormal_check(i, j, wnnprim, inorthotol, inprint, intype)

            orthomat(i, j) = overlap

        end do

    end do

    print*, "All MOs are good"

end subroutine

