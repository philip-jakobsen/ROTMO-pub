!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! module for common variables related to basis functions, orbitals and stuff
!
module orbital_variables

    implicit none

    public

    ! number of frozen orbitals
    integer                                         :: nfc
    
    ! first used in loadbasis
    ! all exponents
    real*8, dimension(:), allocatable               :: prims

! local for each atom!
    ! number of primitives for each atom
    integer                                         :: nprim

    ! number of referenceprimitives for each atom
    integer                                         :: refnprim

    ! label for basis functions on each atom
    character(len=5), dimension(:), allocatable     :: labels

    ! 
    integer, dimension(:), allocatable              :: atoml

    ! angular momentum for basis function on each atom
    integer, dimension(:,:), allocatable            :: atoml2

! global for whole molecule!
    ! total number of primitive functions for all atoms
    integer                                         :: nnprim

    ! same as nnprim for non-obvious reason
    integer                                         :: wnnprim

    ! reference number of primitives on all atoms
    integer                                         :: refnnprim

    ! all exponents
    real*8, dimension(:), allocatable               :: allprim

    ! all reference exponents
    real*8, dimension(:), allocatable               :: refallprim

    ! labels for all primitives
    character(len=5), dimension(:), allocatable     :: alllabels

    ! labels for all reference primitives
    character(len=5), dimension(:), allocatable     :: refalllabels

    ! list of total angular momenta for all basis functions
    integer, dimension(:), allocatable              :: alll

    ! list of total angular momenta for all reference basis functions
    integer, dimension(:), allocatable              :: refalll

    ! center for all basis function
    real*8, dimension(:,:), allocatable             :: allcoords

    ! center for all reference basis function
    real*8, dimension(:,:), allocatable             :: refallcoords

    ! list of angular momentum in x, y, z
    integer, dimension(:,:), allocatable            :: allatoml2

    ! list of reference angular momentum in x, y, z
    integer, dimension(:,:), allocatable            :: refallatoml2

    ! containing atom labels
    character(len=5), dimension(:), allocatable     :: allat

    ! containing reference atom labels
    character(len=5), dimension(:), allocatable     :: refallat

    ! MO coefficients
    real*8, dimension(:,:), allocatable             :: MO

    ! reference MO coefficients
    real*8, dimension(:,:), allocatable             :: refMO

    ! generate components of basis functions given total angular momentum
    integer, dimension(:,:), allocatable            :: cartl                         

    ! overlapmatrix
    real*8, dimension(:,:), allocatable             :: overlapmatrix

    ! density matrix
    real*8, dimension(:,:), allocatable             :: densitymatrix

    ! density matrix after enlargement due to non-zero frozen electrons
    real*8, dimension(:,:), allocatable             :: fulldensitymatrix

    ! reference density matrix
    real*8, dimension(:,:), allocatable             :: refdensitymatrix
    
    ! reference density matrix after enlargement due to non-zero frozen electrons
    real*8, dimension(:,:), allocatable             :: fullrefdensitymatrix

    ! reference density matrix from MRCC
    real*8, dimension(:,:), allocatable             :: mrccdensitymatrix

    ! enlarged density matrix from MRCC
    real*8, dimension(:,:), allocatable             :: fullmrccdensitymatrix

    ! 1-electron density matrix in AO basis from GAMESS
    real*8, dimension(:,:), allocatable             :: gamessAOdensitymatrix

    ! 1-electron CI density matrix in AO basis from GAMESS
    real*8, dimension(:,:), allocatable             :: gamessCIAOdensitymatrix

    ! 1-electron CC density matrix in AO basis from GAMESS
    real*8, dimension(:,:), allocatable             :: gamessCCAOdensitymatrix

    ! 1-electron CI density matrix in MO basis from GAMESS
    real*8, dimension(:,:), allocatable             :: gamessCIMOdensitymatrix

    ! list of density in points from evaldens subroutine
    real*8, dimension(:), allocatable               :: dens

    ! list of density in points from evalrefdens subroutine
    real*8, dimension(:), allocatable               :: refdens

    ! list of density in points from evaUldens2 subroutine
    real*8, dimension(:), allocatable               :: Udens

    ! list of density in points from evalaodens subroutine
    real*8, dimension(:), allocatable               :: aodens

    ! list of reference density in points from evalrefaodens subroutine
    real*8, dimension(:), allocatable               :: refaodens

    ! list of reference density in points from evalrefmodens subroutine
    real*8, dimension(:), allocatable               :: refmodens

    ! list of loaded density in points from external file
    real*8, dimension(:), allocatable               :: extrefdens

    ! copy of udens used in sweep subroutine
    real*8, dimension(:), allocatable               :: udens_copy

    ! threshold for evaluation of density matrix elements
    real*8                                          :: dtol

    ! overlap of two MOs
    real*8                                          :: overlap

    ! matrix of overlap between MOs
    real*8, dimension(:,:), allocatable             :: orthomat
    
    ! tolerance for orthonormality <i|j> = \delta(i,j) +/- tolerance
    real*8                                          :: orthotol

    ! flag for restart sweep with alphas
    logical                                         :: RESTART

    ! symmetry labels
    character(len=10), dimension(:), allocatable    :: symlabels

    ! save double factorials
    integer, dimension(30)                          :: doublefact

    !
    ! moved from sweep_variables
    !

    ! rotated MO
    real*8, dimension(:,:), allocatable             :: UMO

    ! number of doubly occupied orbitals
    integer                                         :: nocc

    ! number of rotation angles connecting occupied and virtual orbitals
    integer                                         :: nangles
    
    !g flag to check orthonormality of MOs after rotation
    logical                                         :: ortho_check

    real*8                                          :: trust

    ! full analytical hessian
    real*8                                          :: hij

    ! flag to only sweep pairs with correct symmetry
    logical                                         :: symcheck

    ! array to hold angles (alphas) after a whole sweep (all occ-virt pairs)
    real*8, dimension(:,:), allocatable             :: converged_alphas

    ! array to hold previous alphas
    real*8, dimension(:,:), allocatable             :: converged_alphas_previous

    ! array to hold loaded alphas
    real*8, dimension(:,:), allocatable             :: converged_alphas_restart

    real*8                                          :: dFdT, dF2dT2

    save

end module

