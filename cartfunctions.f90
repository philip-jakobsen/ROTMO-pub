!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine and function to evaluate cartesian gaussian
! basis function at point (x, y, z) given exponent inalpha
! and type 
! 
! only uncontracted and up to h functions
!
! also only cartesian representation
!
!==========================================================

function evalcartfunc2(inalpha, incartcoords, innxnynz) result(cartbasisfunction)

    use orbital_variables

    implicit none

    ! exponent
    real*8, intent(in)                  :: inalpha

    ! point, generally r-R_a with R_a being coordinates of atom the basis
    ! function resides
    real*8, dimension(3), intent(in)    :: incartcoords

    ! angular momentum (nx,ny,nz)
    integer, dimension(3), intent(in)   :: innxnynz

    ! result
    real*8                              :: cartbasisfunction

    real*8                              :: pi

    ! intermediate results
    real*8                              :: rcartx, rcarty, rcartz

    real*8                              :: x, y, z

    integer                             :: nx, ny, nz


    x = incartcoords(1)
    y = incartcoords(2)
    z = incartcoords(3)

    nx = innxnynz(1)
    ny = innxnynz(2)
    nz = innxnynz(3)

    pi = 4*atan(1.0d0)

    rcartx = (2*inalpha/pi)**(0.25d0)*sqrt(((4.0d0*inalpha)**dble(nx))/(dble(doublefact(2*nx-1+2)))) &
        & * x**dble(nx)*exp(-inalpha*x**2.0d0)

    rcarty = (2*inalpha/pi)**(0.25d0)*sqrt(((4.0d0*inalpha)**dble(ny))/(dble(doublefact(2*ny-1+2)))) &
        & * y**dble(ny)*exp(-inalpha*y**2.0d0)

    rcartz = (2*inalpha/pi)**(0.25d0)*sqrt(((4.0d0*inalpha)**dble(nz))/(dble(doublefact(2*nz-1+2)))) &
        & * z**dble(nz)*exp(-inalpha*z**2.0d0)

    ! assembly
    cartbasisfunction = rcartx*rcarty*rcartz

end function


recursive function doublefactfunc(inn) result(outn)

    implicit none

    integer, intent(in) :: inn

    integer             :: outn

    if (inn .le. 0) then

        outn = 1

    else

        outn = inn * doublefactfunc(inn-2)

    end if

end function doublefactfunc

