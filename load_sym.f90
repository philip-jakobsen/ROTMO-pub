!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to load symmetry labels from GAMESS
!
!==========================================================
subroutine loadsym(insym, indebug)

    use orbital_variables
    use common_variables

    implicit none

    ! input parameters
    character(len=150)                   :: insym

    logical                             :: indebug

    integer                             :: i
    integer :: tmp

    print '(A50)', "Loading symmetry labels"
    print*, "================================================================"
    print*, ""

    allocate(symlabels(wnnprim))

    open(unit=13, file=trim(insym))

    do i=1, wnnprim

        read(13, '(I4, A10)') tmp, symlabels(i)
        
    end do

    close(unit=13)

    if (indebug .eqv. .true.) then

        print "(A8, A15)", "MO", "sym"

        do i=1, wnnprim

            print '(I8, A15)', i, symlabels(i)

        end do

    end if

    print*, ""

end subroutine

