!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! calculate gradient
subroutine calc_grad5(inalphas, UMO_rotated, inocc, ivirt)

    use orbital_variables
    use grid_variables
    use common_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(in)      :: UMO_rotated
    real*8, dimension(npoints) :: grad_points

    real*8, dimension(:,:), allocatable :: Umat_grad, Wmat
    real*8, dimension(:), allocatable :: temp_occ, temp_vir

    integer, intent(in)                     :: inocc, ivirt

    real*8                      :: tmpm
    !real*8                      :: tmpi, tmpm
    !real*8                      :: tmpo1,tmpo2,tmpv1,tmpv2,tmps
    integer                     :: iao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint, k_var, nvir

    real*8                      :: MOmi
    real*8                      :: MOii

    real*8                      :: chii

    real*8                      :: delrho,grw, pref
    integer                     :: iprim, i,j

    dFdT = 0.0d0
    grad_points = 0.0d0

    nvir = wnnprim - nocc
    k_var = (inocc-1)*nvir + (ivirt-nocc)

    allocate(Umat_grad(wnnprim,wnnprim))
    allocate(temp_occ(nocc))
    allocate(temp_vir(nocc))

    call form_umat_grad2(inalphas, k_var, Umat_grad)
    !print*, "FRJ, grad5, Umat_grad2",inocc,ivirt,k_var
    !do i=1,wnnprim
    !  print "(14E12.4)",Umat_grad(i,:)
    !enddo

    allocate(Wmat(wnnprim, wnnprim))
    Wmat = 0.0d0

    ! rotate all the orbitals with the Umat gradient
    do i=1, wnnprim
      do j=1, wnnprim
            do iprim=1, wnnprim
                Wmat(iprim,i) = Wmat(iprim,i) + Umat_grad(i,j)*UMO(iprim, j)
            end do
      end do
    end do

    !$omp parallel do private(chii, MOmi, MOii, iao, grw, pref, delrho, temp_occ, temp_vir, j)

    ! occupied rotated contribution: MOmi
    ! gradient rotated virtual occupied contribution: MOii
    do ipoint=1, npoints
        temp_occ = 0.0d0
        temp_vir = 0.0d0
        do iao=1, wnnprim
            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            do j=1,nocc
              MOmi = UMO_rotated(iao, j)
              MOii = Wmat(iao, j)
              temp_occ(j) = temp_occ(j) + (chii*MOmi)
              temp_vir(j) = temp_vir(j) + (chii*MOii)
            end do
        end do

        grw  = grid(4,ipoint)
        pref = refaodens(ipoint)
        delrho = pref - udens(ipoint)

        tmpm = 0.0d0
        do j=1,nocc
          tmpm = tmpm + temp_occ(j)*temp_vir(j)
        end do
        grad_points(ipoint) = grw*grw*delrho*tmpm
    end do

    !$omp end parallel do

    deallocate(Umat_grad)
    deallocate(Wmat)
    deallocate(temp_occ)
    deallocate(temp_vir)

    ! integrate over points
    dFdT = (-8.0d0 / ErrF_Norm) * sum(grad_points)

end subroutine calc_grad5

subroutine full_hess6(inalphas, UMO_rotated, iocc,ivir,jocc,jvir)

    use orbital_variables
    use grid_variables
    use common_variables

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(in)      :: UMO_rotated
    real*8, dimension(npoints) :: hess_points

    real*8, dimension(:,:), allocatable :: Uimat_grad, Wimat
    real*8, dimension(:,:), allocatable :: Ujmat_grad, Wjmat
    real*8, dimension(:,:), allocatable :: Uiimat_hess, Wiimat
    real*8, dimension(:), allocatable :: temp_iocc, temp_ivir, temp_jvir, temp_ijvir

    real*8                      :: tmp1, tmp2, tmp3, tmp4
    integer                     :: iao, ipoint
    real*8                      :: evalcartfunc2

    integer                     :: iocc,jocc,ivir,jvir
    integer                     :: kj_var,mn_var

    real*8                      :: chii
    real*8                      :: MOmi,MOkj,MOmn, MOkm

    real*8                      :: delrho,grw
    integer                     :: iprim

    dF2dT2 = 0.0d0
    hess_points = 0.0d0

    nvir = wnnprim - nocc
    kj_var = (iocc-1)*nvir + (ivir-nocc)
    mn_var = (jocc-1)*nvir + (jvir-nocc)
    !print*, "FRJ, hess6, index",iocc,ivir,jocc,jvir,kj_var,mn_var

    allocate(Uimat_grad(wnnprim, wnnprim))
    allocate(Ujmat_grad(wnnprim, wnnprim))
    allocate(Uiimat_hess(wnnprim, wnnprim))
    allocate(Wimat(wnnprim, wnnprim))
    allocate(Wjmat(wnnprim, wnnprim))
    allocate(Wiimat(wnnprim, wnnprim))
    allocate(temp_iocc(nocc))
    allocate(temp_ivir(nocc))
    allocate(temp_jvir(nocc))
    allocate(temp_ijvir(nocc))

    call form_umat_grad(inalphas, kj_var, Uimat_grad)
    call form_umat_grad(inalphas, mn_var, Ujmat_grad)
    call form_umat_hess3(inalphas, kj_var, mn_var, Uiimat_hess)

    !print*, "FRJ, Uimat_grad",kj_var
    !do i=1,wnnprim
    !  print "(14E12.4)",Uimat_grad(i,:)
    !enddo
    !print*, "FRJ, Ujmat_grad",mn_var
    !do i=1,wnnprim
    !  print "(14E12.4)",Ujmat_grad(i,:)
    !enddo
    !print*, "FRJ, Uiimat_hess",kj_var,mn_var
    !do i=1,wnnprim
    !  print "(14E12.4)",Uiimat_hess(i,:)
    !enddo

    Wimat = 0.0d0
    Wjmat = 0.0d0
    Wiimat = 0.0d0
    ! rotate all the orbitals
    do i=1, wnnprim
      do j=1, wnnprim
            do iprim=1, wnnprim
                Wimat(iprim,i) = Wimat(iprim,i) + Uimat_grad(i,j)*UMO(iprim, j)
                Wjmat(iprim,i) = Wjmat(iprim,i) + Ujmat_grad(i,j)*UMO(iprim, j)
                Wiimat(iprim,i) = Wiimat(iprim,i) + Uiimat_hess(i,j)*UMO(iprim, j)
            end do
      end do
    end do

    !$omp parallel do private(temp_iocc, temp_ivir, temp_jvir, temp_ijvir, & 
    !$omp & iao, chii, MOmi, MOkj, MOmn, MOkm, grw, pref, delrho, tmp1, tmp2, tmp3, tmp4)

    do ipoint=1, npoints
        temp_iocc = 0.0d0
        temp_ivir = 0.0d0
        temp_jvir = 0.0d0
        temp_ijvir = 0.0d0
        do iao=1, wnnprim
            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao))
            do j=1,nocc
              MOmi = UMO_rotated(iao, j)
              MOkj = Wimat(iao, j)
              MOmn = Wjmat(iao, j)
              MOkm = Wiimat(iao, j)
              temp_iocc(j) = temp_iocc(j) + (chii*MOmi)
              temp_ivir(j) = temp_ivir(j) + (chii*MOkj)
              temp_jvir(j) = temp_jvir(j) + (chii*MOmn)
              temp_ijvir(j) = temp_ijvir(j) + (chii*MOkm)
            end do
        end do

        tmp1 = 0.0d0
        tmp2 = 0.0d0
        tmp3 = 0.0d0
        tmp4 = 0.0d0
        do j=1,nocc
          tmp1 = tmp1 + temp_iocc(j)*temp_ivir(j)
          tmp2 = tmp2 + temp_iocc(j)*temp_jvir(j)
          tmp3 = tmp3 + temp_ivir(j)*temp_jvir(j)
          tmp4 = tmp4 + temp_iocc(j)*temp_ijvir(j)
        end do

        grw  = grid(4,ipoint)
        pref = refaodens(ipoint)
        delrho = pref - udens(ipoint)

        hess_points(ipoint) = -4.0d0*tmp1*tmp2 + delrho*(tmp3+tmp4)
        hess_points(ipoint) = grw*grw*hess_points(ipoint)

    end do

    !$omp end parallel do

    deallocate(Uimat_grad)
    deallocate(Ujmat_grad)
    deallocate(Uiimat_hess)
    deallocate(Wimat)
    deallocate(Wjmat)
    deallocate(Wiimat)
    deallocate(temp_iocc)
    deallocate(temp_ivir)
    deallocate(temp_jvir)
    deallocate(temp_ijvir)

    ! sum over points
    hij = (-8.0d0 / ErrF_Norm) * sum(hess_points)
    
end subroutine full_hess6


! calculate unitary transformation matrix Umat from a skew-symmetric X-matrix
! containing the rotation angles
subroutine form_umat(inalphas, Umat)

    use orbital_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat

    real*8, dimension(:,:), allocatable :: Xmat,Vmat,Wmat

    real*8                      :: eps,tfac,vmax
    integer                     :: kmax,kiter
    integer                     :: i,j,icount

    !print*, "FRJ, entry   form_Umat"
    !print "(14E12.4)",inalphas
    ! allocate and initialize working matrices
    allocate(Xmat(wnnprim, wnnprim))
    allocate(Vmat(wnnprim, wnnprim))
    allocate(Wmat(wnnprim, wnnprim))

    Umat = 0.0d0
    Xmat = 0.0d0
    Vmat = 0.0d0
    Wmat = 0.0d0

    forall(i = 1:wnnprim) Umat(i,i) = 1.0d0

    ! fill the Xmatrix using the nocc*(wnnprim-nocc) rotations angles in inalphas
    icount = 0
    do i=1,nocc
      do j=nocc+1,wnnprim
        icount = icount + 1
        Xmat(i,j) =  inalphas(icount)
      enddo
    enddo
    ! make is skew-symmetric
    do i=1,nocc
      do j=nocc+1,wnnprim
        Xmat(j,i) = -Xmat(i,j)
      enddo
    enddo

    ! initialize, always form U as I + X, then add terms until converged
    Vmat = Xmat
    Umat = Umat + Xmat

    ! form Umat by Taylor series, limit to kmax terms, terminate when converged
    ! to eps. Vmat = Xmat^n
    kmax = 50
    eps  = 1.0d-14

    do kiter=2, kmax
      tfac = 1.0d0/dble(kiter)
      Wmat = matmul(Vmat,Xmat)
      Vmat = Wmat*tfac
      vmax = maxval(abs(Vmat))
      !print*, "FRJ, form_Umat, iterating, vmax",kiter,vmax
      if (vmax > eps) then
        Umat = Umat + Vmat
        if (kiter == kmax) then
          print*, "form_Umat hitting kmax, bombing",vmax,eps
          stop
        endif
      else
        exit
      endif
    end do

    deallocate(Xmat)
    deallocate(Vmat)
    deallocate(Wmat)

end subroutine form_umat

! calculate unitary transformation matrix Umat from a skew-symmetric X-matrix
! containing the rotation angles
subroutine form_umat2(inalphas, Umat)

    use orbital_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat

    real*8, dimension(:,:), allocatable :: Xmat,Vmat

    real*8                      :: eps,ffac,vmax
    integer                     :: kmax,kiter
    integer                     :: i,j,icount

    allocate(Xmat(wnnprim, wnnprim))
    allocate(Vmat(wnnprim, wnnprim))

    Umat = 0.0d0
    forall(i = 1:wnnprim) Umat(i,i) = 1.0d0

    Xmat = 0.0d0
    ! fill the Xmatrix using the nocc*(wnnprim-nocc) rotations angles in inalphas
    icount = 0
    do i=1,nocc
      do j=nocc+1,wnnprim
        icount = icount + 1
        Xmat(i,j) =  inalphas(icount)
      enddo
    enddo
    ! make is skew-symmetric
    do i=1,nocc
      do j=nocc+1,wnnprim
        Xmat(j,i) = -Xmat(i,j)
      enddo
    enddo

    ! initialize, always form U as I + X, then add terms until converged
    Umat = Umat + Xmat

    ! form Umat by Taylor series, limit to kmax terms, terminate when
    ! converged to eps.
    kmax = 50
    eps  = 1.0d-14

    ffac = 1.0d0
    do kiter=2, kmax
      ffac = ffac*dble(kiter)
      Vmat = 0.0d0
      call form_mat_power(wnnprim, kiter, Xmat, Vmat)
      Vmat = Vmat/ffac
      vmax = maxval(abs(Vmat))
      if (vmax > eps) then
        Umat = Umat + Vmat
        if (kiter == kmax) then
          print*, "form_Umat hitting kmax, bombing",vmax,eps
          stop
        endif
      else
        exit
      endif
    end do

    deallocate(Xmat)
    deallocate(Vmat)

end subroutine form_umat2

! calculate the derivative of the U matrix numerically wrt to a single entry
subroutine form_umat_grad(inalphas, k_grad, Umat_grad)

    use orbital_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat_grad

    real*8, dimension(:,:), allocatable :: Vmat,Wmat
    real*8, dimension(:), allocatable   :: Xvec

    integer                     :: k_grad
    real*8                      :: step
    !integer                     :: i

    ! allocate and initialize working matrices
    allocate(Vmat(wnnprim, wnnprim))
    allocate(Wmat(wnnprim, wnnprim))
    allocate(Xvec(nangles))

    Umat_grad = 0.0d0
    Vmat = 0.0d0
    Wmat = 0.0d0
    step = 1.0d-8
    Xvec = inalphas

    ! double numerical differentiation
    Xvec(k_grad) = Xvec(k_grad) + step
    call form_umat(Xvec, Vmat)
    Xvec(k_grad) = Xvec(k_grad) - 2.0d0*step
    call form_umat(Xvec, Wmat)
    Xvec(k_grad) = Xvec(k_grad) + step

    !print*, "FRJ, Umat_grad num +",k_grad
    !do i=1,wnnprim
    !  print "(14E12.4)",Vmat(i,:)
    !enddo
    !print*, "FRJ, Umat_grad num -",k_grad
    !do i=1,wnnprim
    !  print "(14E12.4)",Wmat(i,:)
    !enddo

    Umat_grad = (Vmat - Wmat) / (2.0d0*step)

    !print*, "FRJ, Umat_grad diff",k_grad
    !do i=1,wnnprim
    !  print "(14E12.4)",Umat_grad(i,:)
    !enddo

    deallocate(Vmat)
    deallocate(Wmat)
    deallocate(Xvec)

end subroutine form_umat_grad

! calculate the second derivative of the U matrix numerically wrt to a two variable
subroutine form_umat_hess(inalphas, k_var, m_var, Umat_hess)

    use orbital_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat_hess

    real*8, dimension(:,:), allocatable :: Vmat,Wmat
    real*8, dimension(:), allocatable   :: Xvec

    integer                     :: k_var, m_var
    real*8                      :: step

    ! allocate and initialize working matrices
    allocate(Vmat(wnnprim, wnnprim))
    allocate(Wmat(wnnprim, wnnprim))
    allocate(Xvec(nangles))

    Umat_hess = 0.0d0
    Vmat = 0.0d0
    Wmat = 0.0d0
    ! NOTE: the results are very sensitive to the stepsize
    ! 10-7 gives identical zero matrix!, 10-3 seems reasonable, at least when
    ! using 10-8 in Umat_grad
    step = 1.0d-3
    Xvec = inalphas

    ! numerical differentiation of the Umat gradient
    Xvec(m_var) = Xvec(m_var) + step
    call form_umat_grad(Xvec, k_var, Vmat)
    Xvec(m_var) = Xvec(m_var) - 2.0d0*step
    call form_umat_grad(Xvec, k_var, Wmat)
    Xvec(m_var) = Xvec(m_var) + step

    Umat_hess = (Vmat - Wmat) / (2.0d0*step)

    deallocate(Vmat)
    deallocate(Wmat)
    deallocate(Xvec)

end subroutine form_umat_hess

! calculate the second derivative of the U matrix numerically wrt to a two variable
subroutine form_umat_hess2(inalphas, k_var, m_var, Umat_hess)

    use orbital_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat_hess

    real*8, dimension(:,:), allocatable :: Vmat,Wmat
    real*8, dimension(:), allocatable   :: Xvec

    integer                     :: k_var, m_var
    real*8                      :: step

    ! allocate and initialize working matrices
    allocate(Vmat(wnnprim, wnnprim))
    allocate(Wmat(wnnprim, wnnprim))
    allocate(Xvec(nangles))

    Umat_hess = 0.0d0
    Vmat = 0.0d0
    Wmat = 0.0d0
    ! NOTE: the step size is less sensitive when the Umat_grad is calculated
    ! from the Taylor expansion
    step = 1.0d-8
    Xvec = inalphas

    ! numerical differentiation of the Umat gradient
    Xvec(m_var) = Xvec(m_var) + step
    call form_umat_grad2(Xvec, k_var, Vmat)
    Xvec(m_var) = Xvec(m_var) - 2.0d0*step
    call form_umat_grad2(Xvec, k_var, Wmat)
    Xvec(m_var) = Xvec(m_var) + step

    Umat_hess = (Vmat - Wmat) / (2.0d0*step)

    deallocate(Vmat)
    deallocate(Wmat)
    deallocate(Xvec)

end subroutine form_umat_hess2

! calculate the second derivative of the U matrix wrt to two variables using he
! Taylor expansion
subroutine form_umat_hess3(inalphas, k_var, m_var, Umat_hess)

    use orbital_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat_hess

    real*8, dimension(:,:), allocatable :: Xmat,Ymat,Zmat,Vmat
    real*8, dimension(:,:), allocatable :: X1mat,X2mat,X3mat,W1mat,W2mat
    real*8                      :: eps,ffac,vmax

    integer                     :: kmax,kiter
    integer                     :: k_var, m_var
    integer                     :: i,j,icount
    integer                     :: nterms,mterms,lterms

    ! allocate and initialize working matrices
    allocate(X1mat(wnnprim, wnnprim))
    allocate(X2mat(wnnprim, wnnprim))
    allocate(X3mat(wnnprim, wnnprim))
    allocate(Ymat(wnnprim, wnnprim))
    allocate(Zmat(wnnprim, wnnprim))
    allocate(Xmat(wnnprim, wnnprim))
    allocate(Vmat(wnnprim, wnnprim))
    allocate(W1mat(wnnprim, wnnprim))
    allocate(W2mat(wnnprim, wnnprim))

    ! fill the Xmatrix using the nocc*(wnnprim-nocc) rotations angles in inalphas
    ! fill the Ymatrix (derivative of Xmat wrt k_var): 1 and -1 for the k_var
    ! fill the Zmatrix (derivative of Xmat wrt m_var): 1 and -1 for the m_var
    Xmat = 0.0d0
    Ymat = 0.0d0
    Zmat = 0.0d0
    icount = 0
    !print*, "FRJ, hess3, inalpha",inalphas
    do i=1,nocc
      do j=nocc+1,wnnprim
        icount = icount + 1
        Xmat(i,j) =  inalphas(icount)
        if (icount == k_var) then
          Ymat(i,j) = 1.0d0
        endif
        if (icount == m_var) then
          Zmat(i,j) = 1.0d0
        endif
      enddo
    enddo
    ! make them skew-symmetric
    do i=1,nocc
      do j=nocc+1,wnnprim
        Xmat(j,i) = -Xmat(i,j)
        Ymat(j,i) = -Ymat(i,j)
        Zmat(j,i) = -Zmat(i,j)
      enddo
    enddo
    !print*, "FRJ, hess3, X_hess3",k_var,m_var
    !do i=1,wnnprim
    !  print "(14E12.4)",Xmat(i,:)
    !enddo
    !print*, "FRJ, hess3, Y_hess3",k_var,m_var
    !do i=1,wnnprim
    !  print "(14E12.4)",Ymat(i,:)
    !enddo
    !print*, "FRJ, hess3, Z_hess3",k_var,m_var
    !do i=1,wnnprim
    !  print "(14E12.4)",Zmat(i,:)
    !enddo

    ! initialize Umat_hess as 0, then add terms until converged
    Umat_hess = 0.0d0

    ! form Umat_hess by Taylor series, limit to kmax terms, terminate when converged to eps. 
    ! we need all combinations of X^n * Y * X^l * Z * X^m with n+m+l-2 = kiter
    ! and the combination with Y and Z interchanged
    ! X1mat = Xmat^n before  Y,Z
    ! X2mat = Xmat^l between Y,Z
    ! X3mat = Xmat^m after   Y,Z
    kmax = 50
    eps  = 1.0d-14

    ffac = 1.0d0
    do kiter=2, kmax
      ffac = ffac*dble(kiter)
      Vmat = 0.0d0
      do nterms = 0,kiter-2
        call form_mat_power(wnnprim, nterms, Xmat, X1mat)
        do mterms = 0,kiter-2-nterms
          lterms = kiter - nterms - mterms - 2
          !print*, "FRJ, hess3, powers",nterms,mterms,lterms
          call form_mat_power(wnnprim, lterms, Xmat, X2mat)
          call form_mat_power(wnnprim, mterms, Xmat, X3mat)
          W1mat = matmul(Zmat,X3mat)
          W2mat = matmul(X2mat,W1mat)
          W1mat = matmul(Ymat,W2mat)
          W2mat = matmul(X1mat,W1mat)
          Vmat = Vmat + W2mat
          W1mat = matmul(Ymat,X3mat)
          W2mat = matmul(X2mat,W1mat)
          W1mat = matmul(Zmat,W2mat)
          W2mat = matmul(X1mat,W1mat)
          Vmat = Vmat + W2mat
        end do
      end do
      Vmat = Vmat/ffac
      vmax = maxval(abs(Vmat))
      !print*, "FRJ, form_Umat_hess3, iterating, vmax",kiter,vmax
      if (vmax > eps) then
        Umat_hess = Umat_hess + Vmat
        if (kiter == kmax) then
          print*, "form_Umat_hess3 hitting kmax, bombing",vmax,eps
          stop
        endif
      else
        exit
      endif
    end do

    deallocate(Xmat)
    deallocate(Ymat)
    deallocate(Zmat)
    deallocate(Vmat)
    deallocate(X1mat)
    deallocate(X2mat)
    deallocate(X3mat)
    deallocate(W1mat)
    deallocate(W2mat)

end subroutine form_umat_hess3


! calculate the derivative of the U matrix wrt to a single entry
! calculated from the derivative of the Taylor expansion
! the results match those calculated by Umat_grad as numerical derivative
subroutine form_umat_grad2(inalphas, k_var, Umat_grad)

    use orbital_variables

    implicit none

    real*8, dimension(nangles), intent(in)              :: inalphas
    real*8, dimension(wnnprim,wnnprim), intent(out)     :: Umat_grad

    real*8, dimension(:,:), allocatable :: Xmat,Ymat,Vmat
    real*8, dimension(:,:), allocatable :: X1mat,X2mat,W1mat,W2mat

    real*8                      :: eps,ffac,vmax
    integer                     :: kmax,kiter
    integer                     :: i,j,icount,k_var
    integer                     :: nterms,mterms

    ! allocate and initialize working matrices
    allocate(Ymat(wnnprim, wnnprim))
    allocate(Xmat(wnnprim, wnnprim))
    allocate(Vmat(wnnprim, wnnprim))
    allocate(X1mat(wnnprim, wnnprim))
    allocate(X2mat(wnnprim, wnnprim))
    allocate(W1mat(wnnprim, wnnprim))
    allocate(W2mat(wnnprim, wnnprim))

    Umat_grad = 0.0d0
    Xmat = 0.0d0
    Ymat = 0.0d0

    ! fill the Xmatrix using the nocc*(wnnprim-nocc) rotations angles in inalphas
    ! fill the Ymatrix (derivative of Xmat): 1 and -1 for the k_var
    icount = 0
    do i=1,nocc
      do j=nocc+1,wnnprim
        icount = icount + 1
        Xmat(i,j) =  inalphas(icount)
        if (icount == k_var) then
          Ymat(i,j) = 1.0d0
        endif
      enddo
    enddo
    ! make them skew-symmetric
    do i=1,nocc
      do j=nocc+1,wnnprim
        Xmat(j,i) = -Xmat(i,j)
        Ymat(j,i) = -Ymat(i,j)
      enddo
    enddo

    ! initialize Umat_grad as Y, then add terms until converged
    Umat_grad = Ymat

    ! form Umat_grad by Taylor series, limit to kmax terms, terminate when converged to eps. 
    ! we need all combinations of X^n * Y * X^m with n+m-1 = kiter
    ! X1mat = Xmat^n before Xp
    ! X2mat = Xmat^m after  Xp
    kmax = 50
    eps  = 1.0d-14

    ffac = 1.0d0
    do kiter=2, kmax
      ffac = ffac*dble(kiter)
      Vmat = 0.0d0
      do nterms = 0,kiter-1
        mterms = kiter - nterms - 1
        call form_mat_power(wnnprim, nterms, Xmat, X1mat)
        call form_mat_power(wnnprim, mterms, Xmat, X2mat)
        W1mat = matmul(Ymat,X2mat)
        W2mat = matmul(X1mat,W1mat)
        Vmat = Vmat + W2mat
      end do
      Vmat = Vmat/ffac
      vmax = maxval(abs(Vmat))
      !print*, "FRJ, form_Umat_grad2, iterating, vmax",kiter,vmax
      if (vmax > eps) then
        Umat_grad = Umat_grad + Vmat
        if (kiter == kmax) then
          print*, "form_Umat_grad2 hitting kmax, bombing",vmax,eps
          stop
        endif
      else
        exit
      endif
    end do

    deallocate(Xmat)
    deallocate(Ymat)
    deallocate(Vmat)
    deallocate(X1mat)
    deallocate(X2mat)
    deallocate(W1mat)
    deallocate(W2mat)

end subroutine form_umat_grad2

! calculate the n'th product of a matrix by itself
subroutine form_mat_power(mdim, npower, Vmat, Wmat)

    implicit none

    real*8, dimension(mdim,mdim), intent(in)            :: Vmat
    real*8, dimension(mdim,mdim), intent(out)           :: Wmat

    real*8, dimension(:,:), allocatable :: Xmat

    integer                     :: n,mdim,npower

    if (npower == 0) then
      Wmat = 0.0d0
      forall(n = 1:mdim) Wmat(n,n) = 1.0d0
    else if (npower == 1) then
      Wmat = Vmat
    else
      allocate(Xmat(mdim,mdim))
      Xmat = Vmat
      do n = 2,npower
        Wmat = matmul(Vmat,Xmat)
        Xmat = Wmat
      end do
      deallocate(Xmat)
    endif

end subroutine form_mat_power

! calculate ErrF by gradient style integration
subroutine calc_ErrF_byint(UMO_rotated)

    use orbital_variables
    !use sweep_variables
    use grid_variables
    use common_variables

    implicit none

    real*8, dimension(wnnprim,wnnprim), intent(in)      :: UMO_rotated
    real*8, dimension(npoints) :: grad_points
    !real*8, dimension(:), allocatable :: tmpm

    real*8                      :: tmp1,tmp2,tmp3
    real*8                      :: tmps
    real*8                      :: nel
    real*8                      :: evalcartfunc2
    real*8                      :: MOmi
    real*8                      :: chii
    real*8                      :: delrho,grw, pref
    integer                     :: iao
    integer                     :: ipoint
    !integer                     :: inocc

    grad_points = 0.0d0
    !allocate(tmpm(nocc))

    !print*, "FRJ, UMO_rotated"
    !do i=1,wnnprim
    !  print "(14E12.4)",UMO_rotated(i,:)
    !enddo

    !$omp parallel do private(tmp1, tmp2, tmp3, chii, MOmi, iao, ipoint, grw, pref, delrho)
    do ipoint=1, npoints
    !    tmpm = 0.0d0
        tmp1 = 0.0d0
        tmp2 = 0.0d0
        tmp3 = 0.0d0
        do iao=1, wnnprim
            chii = evalcartfunc2(allprim(iao), grid(1:3,ipoint)- allcoords(:,iao), allatoml2(:,iao)) 
            MOmi = UMO_rotated(iao, 1)
            tmp1 = tmp1 + (chii*MOmi)
            MOmi = UMO_rotated(iao, 2)
            tmp2 = tmp2 + (chii*MOmi)
            MOmi = UMO_rotated(iao, 3)
            tmp3 = tmp3 + (chii*MOmi)
    !        do inocc = 1,nocc
    !          MOmi = UMO_rotated(iao, inocc)
    !          tmpm(inocc) = tmpm(inocc) + (chii*MOmi)
    !        end do
        end do

    !    tmps = 0.0d0
    !    do inocc = 1,nocc
    !      tmps = tmps + tmpm(inocc)*tmpm(inocc)
    !    end do
    !    tmps = 2.0d0*tmps
        tmps = 2.0d0*(tmp1*tmp1 + tmp2*tmp2 + tmp3*tmp3)

        grw  = grid(4,ipoint)
        pref = refaodens(ipoint)
        !delrho = pref - udens(ipoint)
        delrho = pref - tmps

        !grad_points(ipoint) = tmps*grw
        grad_points(ipoint) = (grw*delrho)**2
    end do
    !$omp end parallel do
    !deallocate(tmpm)

    ! integrate over points
    nel = sum(grad_points)
    !print*, "FRJ, ErrF_byint, nel",nel
    print*, "FRJ, ErrF_byint, ErrF",nel/ErrF_Norm

end subroutine calc_ErrF_byint



