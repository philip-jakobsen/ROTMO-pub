MKLROOT = /comm/swstack/core/mkl/2019.2.187/mkl/

### gfortran
#FASTFLAGS = -ffast-math -O3 -march='westmere' # q12
FASTFLAGS = -ffast-math -O3 -march='sandybridge' # q16, q20
#FASTFLAGS = -ffast-math -O3 -march='skylake' # q36, q40, qfat

FFLAGS = -ffree-form  -Wall -g -fbacktrace -fcheck=all -fopenmp -std=gnu

LDLIBS = philip.a

#FC = ifort
FC = gfortran

default: rotmo.x

clean: 
	rm -f *.o rotmo.x *.mod *.txt philip.a

# MODULES
MODULES = grid_variables.o orbital_variables.o \
		  common_variables.o 

# objects containing subroutines
OBJS = parse_input.o load_lebedev.o integ.o nel.o\
	   construct_grid.o load_cmo.o cartfunctions.o\
	   load_basis.o load_1eD.o load_overlap.o errf.o\
	   dens.o ortho.o load_sym.o \
	   restart.o grad.o  \
	   opt_interface.o deriv.o rotmo.o

# compile all objects
%.o: %.f90
	$(FC) $(FFLAGS) $(FASTFLAGS) -c $*.f90

# link main program
rotmo.x: $(MODULES) $(OBJS)
	./library.make
	$(FC) $(FFLAGS) $(FASTFLAGS) -o rotmo.x rotmo.o $(LDLIBS)

