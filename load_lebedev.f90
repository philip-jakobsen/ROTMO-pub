!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to load lebedev grid from file
!
!==========================================================

subroutine agrid(innagrid)

    use grid_variables

    use common_variables

    implicit none

    integer                             :: i

    integer                             :: innagrid

    real*8                              :: pi, sumw

    character(len=150)                   :: angular_grid_filename

    pi = 4*atan(1.0d0)

    allocate(lebedev_x(innagrid))
    allocate(lebedev_y(innagrid))
    allocate(lebedev_z(innagrid))
    allocate(lebedev_w(innagrid))

    lebedev_x = 0.0d0
    lebedev_y = 0.0d0
    lebedev_z = 0.0d0
    lebedev_w = 0.0d0

    write(angular_grid_filename, *) innagrid

    print*, "================================================================"
    print*, "Loading angular grid from file"
    print*, "================================================================"

    !open(unit=15, file='/home/philip/speciale/program/angular_grid/' // adjustl(angular_grid_filename))

    ! changing this for Frank
    open(unit=15, file = trim(basisdir) // adjustl(angular_grid_filename))

    ! read first line
    read(15, *)

    sumw = 0.0d0
    do i=1, innagrid
        read(15, '(4E24.16)') lebedev_x(i), lebedev_y(i), lebedev_z(i), lebedev_w(i)
        sumw = sumw + lebedev_w(i)
        !print '(E20.10)', lebedev_w(i)
    end do
    close(unit=15)

    ! scale lebedev weights accordingly 
    do i=1, innagrid
        lebedev_w(i) = (4*pi)/sumw * lebedev_w(i)
    end do

end subroutine agrid

