!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================

! evaluate density on grid in mo basis for reference
! result: refmodens
subroutine evalrefmodens(indtol, inwrite)

    use orbital_variables

    use grid_variables

    implicit none

    ! input parameters
    logical                     :: inwrite
    real*8                      :: indtol

    real*8                      :: tmp, tmpm, tmpn
    integer                     :: imo, jmo, mao, nao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: refmo_start_time, refmo_finish_time

    print*, "================================================================"
    print*, "evalrefmodens speaking"
    print*, "Evaluating reference densities on grid"
    print*, ""

    print*, "Starting moref timing"

    call cpu_time(refmo_start_time)
    
    allocate(refmodens(npoints))

    refmodens = 0.0d0

    !$omp parallel do private(tmp, tmpm, tmpn, ipoint)

    do ipoint=1, npoints

        tmp = 0.0d0
        refmodens(ipoint) = 0.0d0

        do imo=1, refnnprim

            do jmo=1, refnnprim

                ! early exit if Dij is less than threshold
                if (abs(gamessCIMOdensitymatrix(imo, jmo)) .le. indtol) then

                    cycle

                end if
                
                tmpm = 0.0d0

                do mao=1, refnnprim

                    tmpm = tmpm + evalcartfunc2(refallprim(mao), grid(1:3,ipoint) - refallcoords(:,mao), &
                        & refallatoml2(:,mao)) * refMO(mao, imo)

                end do

                tmpn = 0.0d0

                do nao=1, refnnprim

                    tmpn = tmpn + evalcartfunc2(refallprim(nao), grid(1:3,ipoint) - refallcoords(:,nao), &
                        & refallatoml2(:,nao)) * refMO(nao, jmo)

                end do

                tmp = tmp + tmpm*tmpn*gamessCIMOdensitymatrix(imo, jmo)

            end do
        end do

        refmodens(ipoint) = tmp

    end do

    !$omp end parallel do

    call cpu_time(refmo_finish_time)
    print '(A20, f12.2)', "Walltime [s] = ", refmo_finish_time - refmo_start_time 

    print*, "================================================================"
    print '(A50)', "Calculated reference densities"
    print*, ""
    
    ! write to file fort.15
    if (inwrite .eqv. .true.) then

        do ipoint=1, npoints

            write(18, '(5E24.16)') grid(1:3,ipoint), refmodens(ipoint), grid(4,ipoint)
        
        end do

    end if

    if (inwrite .eqv. .true.) then

        print*, "Wrote xyz points, reference densities in MO basis and integration weights to fort.18"

    end if

end subroutine evalrefmodens

! evaluate density on grid in ao basis for reference
! result: refaodens
subroutine evalrefaodens(indtol, inwrite)

    use orbital_variables

    use grid_variables

    implicit none

    ! input parameters
    logical                     :: inwrite
    real*8                      :: indtol

    real*8                      :: tmp
    integer                     :: iao, jao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: refao_start_time, refao_finish_time

    print*, "evalrefaodens speaking"

    print*, "Starting aoref timing"

    call cpu_time(refao_start_time)

    allocate(refaodens(npoints))

    refaodens = 0.0d0

    !$omp parallel do private(tmp, iao, jao)

    do ipoint=1, npoints

        tmp = 0.0d0

        do iao=1, refnnprim

            do jao=1, refnnprim

                ! early exit if density matrix element is below threshold
                if (abs(gamessCIAOdensitymatrix(iao, jao)) .le. indtol) then

                    cycle 

                end if

                tmp = tmp + (evalcartfunc2(refallprim(iao), grid(1:3,ipoint) - refallcoords(:,iao), refallatoml2(:,iao))) &
                    & * gamessCIAOdensitymatrix(iao, jao) * & 
                    & (evalcartfunc2(refallprim(jao), grid(1:3,ipoint) - refallcoords(:,jao), refallatoml2(:,jao)))

            end do

        end do
                           
        refaodens(ipoint) = tmp

    end do 

    !$omp end parallel do

    call cpu_time(refao_finish_time)


    print*, "================================================================"
    print '(A50)', "Calculated reference  densities in AO basis"
    print '(A20, f12.2)', "refao walltime [s] = ", refao_finish_time - refao_start_time
    print*, ""

    if (inwrite .eqv. .true.) then

        do ipoint=1, npoints

            write(19, '(5E24.16)') grid(1:3,ipoint), refaodens(ipoint), grid(4,ipoint)

        end do
        
        print*, "Wrote xyz points, reference densities in AO basis and integration weights to fort.19"

    end if

end subroutine


! evaluate density on grid in AO basis
! NOT for reference
! output: list aodens
subroutine evalaodens(indtol, inwrite)

    use orbital_variables

    use grid_variables

    implicit none

    ! input parameters
    logical                     :: inwrite
    real*8                      :: indtol

    real*8                      :: tmp
    integer                     :: iao, jao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    allocate(aodens(npoints))

    aodens = 0.0d0

    !$omp parallel do private(tmp, iao, jao)

    do ipoint=1, npoints

        tmp = 0.0d0

        do iao=1, wnnprim

            do jao=1, wnnprim

                ! early exit if density matrix element is below threshold
                if (abs(gamessAOdensitymatrix(iao, jao)) .le. indtol) then

                    cycle 

                end if

                tmp = tmp + (evalcartfunc2(allprim(iao), grid(1:3,ipoint) - allcoords(:,iao), allatoml2(:,iao))) &
                    & * gamessAOdensitymatrix(iao, jao) * & 
                    & (evalcartfunc2(allprim(jao), grid(1:3,ipoint) - allcoords(:,jao), allatoml2(:,jao)))

            end do

        end do
                           
        aodens(ipoint) = tmp

    end do 

    !$omp end parallel do

    print*, "================================================================"
    print '(A50)', "Calculated densities"
    print*, ""

    if (inwrite .eqv. .true.) then

        do ipoint=1, npoints

            write(17, '(5E24.16)') grid(1:3,ipoint), aodens(ipoint), grid(4,ipoint)

        end do
        
        print*, "Wrote xyz points, densities in AO basis and integration weights to fort.17"

    end if

end subroutine

! evaluate density on grid in MO basis
! result: dens
subroutine evaldens(indtol, inwrite)

    use orbital_variables

    use grid_variables

    implicit none

    ! input parameters
    logical                     :: inwrite
    real*8                      :: indtol

    real*8                      :: tmp
    integer                     :: imo, mao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8 :: dens_start_time, dens_finish_time

    call cpu_time(dens_start_time)

    allocate(dens(npoints))
    dens = 0.0d0

    ! loop over MOs
    do imo=1, wnnprim

        if (densitymatrix(imo,imo) .lt. indtol) then

            cycle

        end if

        print "(A20,I3,A10,E10.4)", "MO index", imo, "D(i,i)", densitymatrix(imo,imo)

        !$omp parallel do private(tmp)

        do ipoint=1, npoints

            tmp = 0.0d0

            do mao=1, wnnprim
                tmp = tmp + (evalcartfunc2(allprim(mao), grid(1:3,ipoint) - allcoords(:,mao), allatoml2(:,mao)) &
                        & *MO(mao, imo))

            end do
                               
            dens(ipoint) = dens(ipoint) + tmp**2*densitymatrix(imo, imo)

        end do

        !$omp end parallel do

    end do 

    call cpu_time(dens_finish_time)

    print*, "================================================================"
    print '(A50)', "Calculated densities"
    print '(A20, f12.2)', "dens walltime [s] = ", dens_finish_time - dens_start_time
    print*, ""

    if (inwrite .eqv. .true.) then

        do ipoint=1, npoints

            write(14, '(5E24.16)') grid(1:3,ipoint), dens(ipoint), grid(4,ipoint)

        end do
        
        print*, "Wrote xyz points, densities and integration weights to fort.14"

    end if


end subroutine

! subroutine to evaluate density on grid in MO basis for reference
! result: refdens
subroutine evalrefdens(indtol, inwrite)

    use orbital_variables

    use grid_variables

    implicit none

    ! input parameters
    logical                     :: inwrite
    real*8                      :: indtol

    real*8                      :: tmp, tmpm, tmpn
    integer                     :: imo, jmo, mao, nao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    print*, "================================================================"
    print*, "Evaluating reference densities on grid"
    print*, ""
    
    allocate(refdens(npoints))

    refdens = 0.0d0

    !$omp parallel do private(tmp, tmpm, tmpn, ipoint)

    do ipoint=1, npoints

        tmp = 0.0d0

        refdens(ipoint) = 0.0d0

        do imo=1, refnnprim

            do jmo=1, refnnprim

                ! early exit if Dij is less than threshold
                if (abs(fullrefdensitymatrix(imo, jmo)) .le. indtol) then

                    cycle

                end if
                
                tmpm = 0.0d0
                do mao=1, refnnprim

                    tmpm = tmpm + evalcartfunc2(refallprim(mao), grid(1:3,ipoint) - refallcoords(:,mao), &
                        & refallatoml2(:,mao)) * refMO(mao, imo)

                end do

                tmpn = 0.0d0
                do nao=1, refnnprim

                    tmpn = tmpn + evalcartfunc2(refallprim(nao), grid(1:3,ipoint) - refallcoords(:,nao), &
                        & refallatoml2(:,nao)) * refMO(nao, jmo)

                end do

                tmp = tmp + tmpm*tmpn*fullrefdensitymatrix(imo, jmo)

            end do

        end do

        !$omp atomic write
        refdens(ipoint) = tmp

    end do

    !$omp end parallel do

    print*, "================================================================"
    print '(A50)', "Calculated reference densities"
    print*, ""
    
    ! write to file fort.15
    if (inwrite .eqv. .true.) then

        do ipoint=1, npoints

            write(15, '(5E24.16)') grid(1:3,ipoint), refdens(ipoint), grid(4,ipoint)
        
        end do

    end if

    if (inwrite .eqv. .true.) then

        print*, "Wrote xyz points, reference densities and integration weights to fort.15"

    end if

end subroutine evalrefdens



! subroutine to evaluate basis functions and density matrix
! on points with rotated MOs
! only symmetric 1-electron density matrix
!
subroutine evalUdens5(indtol, inwrite, UMO_rotated)

    use orbital_variables
    use grid_variables
    !use sweep_variables

    implicit none

    ! input parameters
    real*8                      :: indtol
    logical                     :: inwrite

    real*8                      :: tmp, tmpm
    integer                     :: imo, mao
    real*8                      :: evalcartfunc2
    integer                     :: ipoint

    real*8                      :: chim
    real*8                      :: MOmi

    real*8, dimension(wnnprim,wnnprim) :: UMO_rotated

    !$omp parallel do private(tmp, tmpm, chim, MOmi, mao)

    do ipoint=1, npoints
        tmp = 0.0d0
        do imo=1, wnnprim
            ! early exit if density matrix element is below threshold
            if (abs(densitymatrix(imo, imo)) .le. indtol) then 
                cycle 
            end if
            ! occupied m
            tmpm = 0.0d0
            do mao=1, wnnprim
                chim = evalcartfunc2(allprim(mao), grid(1:3,ipoint)- allcoords(:,mao), allatoml2(:,mao)) 
                MOmi = UMO_rotated(mao, imo)
                tmpm = tmpm + (chim*MOmi)
            end do
            tmp = tmp + densitymatrix(imo, imo) * tmpm**2
        end do
        Udens(ipoint) = Udens(ipoint) + tmp
    end do

    !$omp end parallel do

    if (inwrite .eqv. .true.) then
        do ipoint=1, npoints
            write(16, '(4E24.16)') grid(1:3,ipoint), Udens(ipoint)
        end do
        print*, "Wrote xyz points and rotated densities to fort.16"
    end if

end subroutine

