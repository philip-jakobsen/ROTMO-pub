!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! module for common variables
!
module common_variables

    implicit none

    public

    ! number of atoms
    integer                                         :: natoms

    ! geometry of molecule
    real*8, dimension(:,:), allocatable             :: coords
    
    ! atomnumbers of atoms in molecule
    integer, dimension(:), allocatable              :: atnums

    ! labels for atoms in molecule
    character(len=5), dimension(:), allocatable     :: atlabel

    ! directory for basis files (in DALTON format)
    character(len=150)                               :: basisdir

    ! filename for basisfile for each atom
    character(len=150), dimension(:), allocatable    :: basisfilename

    ! filename for reference basisfile for each atom
    character(len=150), dimension(:), allocatable    :: refbasisfilename

    ! filename for MO coefficients (name comes from DALTON)
    character(len=150)                               :: mopun

    ! filename for reference MO coefficients (name comes from DALTON)
    character(len=150)                               :: refmopun

    ! filename for overlap matrix file
    character(len=150)                               :: overlapfilename

    ! filename for density matrix
    character(len=150)                               :: densitymatrixfilename

    ! filename for reference density matrix
    character(len=150)                               :: refdensitymatrixfilename

    ! filename for symmetry labels
    character(len=150)                               :: symfilename

    ! flag to perform sweep procedure
    logical                                         :: sweepflag

    ! flag to load reference densities on grid from file
    logical                                         :: loadref

    ! ErrF
    real*8                                          :: ErrF_value

    ! ErrF_Norm
    real*8                                          :: ErrF_Norm

    ! flag to print basis information and GAMESS matrices
    logical                                         :: inp_print

    character(len=5)                                :: opttype

    ! for interface with Franks optimization program
    logical                                         :: frj_interface
    
    ! data root dir
    character(len=50)                               :: data_dir

    ! global flag to write density files
    logical                                         :: writeflag

    save

end module
