!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to restart sweep from previous alphas
!
subroutine restart_alphas()

    use orbital_variables

    implicit none

    integer :: i, j
    
    allocate(converged_alphas_restart(nocc,wnnprim))

    print*, "Loading alphas from restart file fort.90..."
     
    read(90, "(4E24.16)") converged_alphas_restart

    print "(A30)", "Loaded alphas:"
    print "(4E24.16)", converged_alphas_restart

    print*, "Done"

    print*, ""
    print*, "Updating UMO..."

    allocate(UMO(wnnprim,wnnprim))

    UMO = MO

    do i=1, nocc
    
        do j=1, wnnprim
        
            print "(A30,2I5,A10,A10,E24.16)", "Updating pair ", i, j, symlabels(j), "alpha =", converged_alphas_restart(i,j)

            ! update UMO
            ! arguments: <occ MO index ii>, < virtual MO index j>, <list>
            ! output: "updated UMO"
            call update_umos(i, j, converged_alphas_restart(i,j))
        
        end do

    end do

    !print "(E24.16)", UMO(:,1)

    print*, "Done"
    print*, ""

end subroutine restart_alphas

!
! subroutine to load reference densities on grid from file (to avoid doing it
! again)
!
subroutine restart_refdens()

    use common_variables

    use grid_variables

    use orbital_variables

    implicit none

    integer :: i

    ! check if fort.15 exists
    inquire(file="REFDENS", exist = loadref)

    if (loadref .eqv. .true.) then

        print*, "Attempting to load reference densities from file REFDENS"

        allocate(extrefdens(npoints))

        open(unit=25, file="REFDENS")

        do i=1, npoints

            read(25, '(5E24.16)') grid(1:3,i), extrefdens(i), grid(4,i)

        end do

        close(unit=25)

        print*, "Done"

        call sumdens(extrefdens, .true.)

        print*, "Setting refaodens == extrefdens"

        allocate(refaodens(npoints))

        refaodens = extrefdens

    else

        print*, "Evaluating reference FCI densities on grid in AO basis"

        call evalrefaodens(dtol, writeflag)

        ! check number of electrons
        call sumdens(refaodens, .true.)

    end if

end subroutine restart_refdens


! subroutine to update UMOs given a pair 
! i and j of MO indices and an angle alpha
subroutine update_umos(ino, inv, inalpha)

    use orbital_variables

    integer                     :: ino, inv

    real*8                      :: inalpha

    integer                     :: iprim                         

    real*8                      :: tmpio, tmpiv

    do iprim=1, wnnprim

        tmpio = UMO(iprim, ino)
        tmpiv = UMO(iprim, inv)
        
        UMO(iprim,ino) = cos(inalpha)*tmpio + sin(inalpha)*tmpiv

        UMO(iprim,inv) = -sin(inalpha)*tmpio + cos(inalpha)*tmpiv

    end do

end subroutine update_umos

