!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
! subroutine to interface with Franks general optimization program
! input: fort.10
! output: hessian (fort.9), function value and gradient (fort.12)
subroutine opt_interface(debugflag)

    use common_variables
    use orbital_variables
    !use sweep_variables
    use grid_variables

    implicit none

    logical                     :: debugflag
    logical                     :: doderivs

    real*8, dimension(:), allocatable  :: alphas, grad
    real*8, dimension(:,:), allocatable :: hess
    real*8, dimension(:,:), allocatable :: Umat, UMO_rotated

    integer                     :: i, j, iprim

    real*8 :: radius
    integer :: mstep

    !FRJ: when calculating Jee at the final point, do not do derivatives, hardwired at present
    doderivs = .true.
    !doderivs = .false.

    ! read dimension
    read(10, *) nangles

    ! allocation
    allocate(grad(nangles))
    allocate(alphas(nangles))
    allocate(hess(nangles,nangles))

    ! read input parameters
    ! alphas, radius, mstep
    read(10, "(4E24.16)") alphas

    if (debugflag .eqv. .true.) then
        print*, "input alphas"
        print "(4E24.16)", alphas
    end if

    ! read radius and mstep
    read(10, *) radius, mstep

    if (debugflag .eqv. .true.) then
        print*, "radius mstep"
        print*, radius, mstep
    end if

    ! construct Umat from rotation angles in alphas
    allocate(Umat(wnnprim, wnnprim))
    call form_Umat2(alphas, Umat)

    ! form rotated MOs
    allocate(UMO_rotated(wnnprim, wnnprim))
    UMO_rotated = 0.0d0

    ! rotate all the orbitals
    do i=1, wnnprim
      do j=1, wnnprim
            do iprim=1, wnnprim
                UMO_rotated(iprim,i) = UMO_rotated(iprim,i) + umat(i,j)*UMO(iprim, j)
            end do
      end do
    end do

    ! allocate space for density on grid points
    allocate(Udens(npoints))
    Udens = 0.0d0

    ! get function value
    call evalUdens5(dtol, .false., UMO_rotated)
    call calcerrf(udens, refaodens, .false.)
    call calcVneJee(udens, refaodens)

    if (debugflag .eqv. .true.) print*, "ErrF = ", errf_value

    ! get gradient
    grad = 0.0d0
    if (doderivs .eqv. .true.) then
      call calc_grad_vec(alphas, UMO_rotated, grad)
    end if

    ! write gradient vector
    write(12, "(E24.16)") errf_value
    write(12, "(E24.16)") grad

    if (debugflag .eqv. .true.) then
        print*, "grad"
        print "(E24.16)", grad
    end if

    ! get hessian
    hess = 0.0d0
    if (doderivs .eqv. .true.) then
      call calc_hess_mat(alphas, UMO_rotated, hess)
    end if

    ! per README
    do i=1, nangles
        write(9, "(5d20.10)") (hess(i,j),j=1,nangles)
    end do

    do i=1, nangles
      do j=1, nangles
        print "(2i5,3x,d20.10)",i,j,hess(i,j)
      enddo
    end do

    if (debugflag .eqv. .true.) then
        print*, "hess"
        print "(4E24.16)", hess
    end if

    if (debugflag .eqv. .true.) then
        print*, "Writing rotated UMO to fort.99"
        !write(99, "(4E24.16)") UMO
        ! in same format as load_cmo.f90
        do i=1, wnnprim
            do j=1, wnnprim
                write(99, "(2I4, E24.16)") i, j, UMO(j, i)
            end do
        end do
    end if

    deallocate(Umat)
    deallocate(UMO_rotated)
    deallocate(udens)
    deallocate(grad)
    deallocate(alphas)
    deallocate(hess)


end subroutine opt_interface

