!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! subroutine to load basis in dalton format
! 
! only uncontracted and up to f functions
!
! also only cartesian representation
!
! also assigning cartesian coordinates to 
!
!==========================================================

! subroutine that takes l (angular momentum) as input and makes an array of combinations of
! nx, ny and nz where l = nx+ny+nz
subroutine makel(inl)

    use orbital_variables

    implicit none

    integer, intent(in)                 :: inl

    ! s func
    if (inl .eq. 0) then

        allocate(cartl(3,1))

        cartl = 0
        
    ! p func
    else if (inl .eq. 1) then
        
        allocate(cartl(3,3))

        cartl = 0

        cartl(1,1) = 1
        cartl(2,2) = 1
        cartl(3,3) = 1

    ! d func
    else if (inl .eq. 2) then
        
        allocate(cartl(3,6))

        ! new gamess ordering
        cartl = 0

        ! xx
        cartl(1,1) = 2

        ! yy
        cartl(2,2) = 2

        ! zz
        cartl(3,3) = 2

        ! xy
        cartl(1,4) = 1
        cartl(2,4) = 1

        ! xz
        cartl(1,5) = 1
        cartl(3,5) = 1

        ! yz
        cartl(2,6) = 1
        cartl(3,6) = 1


    ! f func
    else if (inl .eq. 3) then

        allocate(cartl(3,10))

        cartl = 0

        ! xxx
        cartl(1,1) = 3
        
        ! yyy
        cartl(2,2) = 3

        ! zzz
        !cartl(1,3) = 3
        cartl(3,3) = 3

        ! xxy
        cartl(1,4) = 2
        cartl(2,4) = 1

        ! xxz
        cartl(1,5) = 2
        cartl(3,5) = 1

        ! yyx
        cartl(2,6) = 2
        cartl(1,6) = 1

        ! yyz
        cartl(2,7) = 2
        cartl(3,7) = 1

        ! zzx
        cartl(3,8) = 2
        cartl(1,8) = 1

        ! zzy
        cartl(3,9) = 2
        cartl(2,9) = 1

        ! xyz
        cartl(1,10) = 1
        cartl(2,10) = 1
        cartl(3,10) = 1

    ! g func
    else if (inl .eq. 4) then

        allocate(cartl(3,15))

        cartl = 0

        ! xxxx
        cartl(1,1) = 4

        ! yyyy
        cartl(2,2) = 4

        ! zzzz
        cartl(3,3) = 4

        ! xxxy
        cartl(1,4) = 3
        cartl(2,4) = 1

        ! xxxz
        cartl(1,5) = 3
        cartl(3,5) = 1

        ! yyyx
        cartl(2,6) = 3
        cartl(1,6) = 1

        ! yyyz
        cartl(2,7) = 3
        cartl(3,7) = 1

        ! zzzx
        cartl(3,8) = 3
        cartl(1,8) = 1

        ! zzzy
        cartl(3,9) = 3
        cartl(2,9) = 1

        ! xxyy
        cartl(1,10) = 2
        cartl(2,10) = 2

        ! xxzz
        cartl(1,11) = 2
        cartl(3,11) = 2

        ! yyzz
        cartl(2,12) = 2
        cartl(3,12) = 2

        ! xxyz
        cartl(1,13) = 2
        cartl(2,13) = 1
        cartl(3,13) = 1

        ! yyxz
        cartl(1,14) = 1
        cartl(2,14) = 2
        cartl(3,14) = 1

        ! zzxy
        cartl(1,15) = 1
        cartl(2,15) = 1
        cartl(3,15) = 2


    ! h func
    else if (inl .eq. 5) then

        allocate(cartl(3,21))

        cartl = 0

        ! xxxxx
        cartl(1,1) = 5

        ! yyyyy
        cartl(2,2) = 5

        ! zzzzz
        cartl(3,3) = 5

        ! xxxxy
        cartl(1,4) = 4
        cartl(2,4) = 1

        ! xxxxz
        cartl(1,5) = 4
        cartl(3,5) = 1

        ! yyyyx
        cartl(2,6) = 4
        cartl(1,6) = 1

        ! yyyyz
        cartl(2,7) = 4
        cartl(3,7) = 1

        ! zzzzx
        cartl(3,8) = 4
        cartl(1,8) = 1

        ! zzzzy
        cartl(3,9) = 4
        cartl(2,9) = 1

        ! xxxyy
        cartl(1,10) = 3
        cartl(2,10) = 2

        ! xxxzz
        cartl(1,11) = 3
        cartl(3,11) = 2

        ! yyyxx
        cartl(2,12) = 3
        cartl(1,12) = 2

        ! yyyzz
        cartl(2,13) = 3
        cartl(3,13) = 2

        ! zzzxx
        cartl(1,14) = 2
        cartl(3,14) = 3

        ! zzzyy
        cartl(2,15) = 2
        cartl(3,15) = 3

        ! xxxyz
        cartl(1,16) = 3
        cartl(2,16) = 1
        cartl(3,16) = 1

        ! yyyxz
        cartl(1,17) = 1
        cartl(2,17) = 3
        cartl(3,17) = 1

        ! zzzxy
        cartl(1,18) = 1
        cartl(2,18) = 1
        cartl(3,18) = 3

        ! xxyyz
        cartl(1,19) = 2
        cartl(2,19) = 2
        cartl(3,19) = 1

        ! xxzzy
        cartl(1,20) = 2
        cartl(2,20) = 1
        cartl(3,20) = 2

        ! yyzzx
        cartl(1,21) = 1
        cartl(2,21) = 2
        cartl(3,21) = 2

    end if

end subroutine

subroutine readallbasis(inbasisdir, inbasisfilename, indebug)

    use common_variables

    use orbital_variables

    implicit none

    integer                                     :: iatoms
    character(len=150), dimension(natoms)        :: inbasisfilename
    character(len=150)                           :: basis
    character(len=150)                           :: inbasisdir
    character(len=5)                            :: zval
    integer                                     :: i

    real*8, dimension(:), allocatable           :: tmpprim
    character(len=5), dimension(:), allocatable :: tmplabels
    integer, dimension(:), allocatable          :: tmpl
    character(len=5), dimension(:), allocatable :: tmpat

    logical                                     :: indebug

    real*8, dimension(:,:), allocatable         :: tmpcoords
    integer, dimension(:,:), allocatable        :: tmpatoml2

    ! tmp array to hold all primitives
    allocate(tmpprim(1000))
    allocate(tmplabels(1000))
    allocate(tmpl(1000))
    allocate(tmpat(1000))
    allocate(tmpcoords(3,1000))
    allocate(tmpatoml2(3,1000))

    tmpprim = 0.0d0
    tmpl = 0
    nnprim = 0
    tmpatoml2 = 0

    print*, "================================================================"
    print*, "Loading basis files"
    print*, "================================================================"

    do iatoms=1, natoms

        ! stitch together path of basis file
        write(zval, '(I5)') atnums(iatoms)

        basis = trim(inbasisdir) // trim(adjustl(zval)) // "/" // trim(inbasisfilename(iatoms))
        print*, "basis for atom ", atlabel(iatoms), "is ", basis

        call readgaussianbasis(basis, indebug)
        print*, "nprim = ", nprim


        do i=1, nprim

            tmpprim(i+nnprim) = prims(i)
            tmplabels(i+nnprim) = labels(i)
            tmpl(i+nnprim) = atoml(i)

            tmpat(i+nnprim) = atlabel(iatoms)
            tmpcoords(:,i+nnprim) = coords(:,iatoms)
            tmpatoml2(:,i+nnprim) = atoml2(:,i)

        end do

        nnprim = nnprim + nprim

        deallocate(prims)
        deallocate(labels)
        deallocate(atoml)
        deallocate(atoml2)

        print*, "================================================================"

    end do

    allocate(allprim(nnprim))
    allocate(alllabels(nnprim))
    allocate(alll(nnprim))
    allocate(allat(nnprim))
    allocate(allcoords(3,nnprim))
    allocate(allatoml2(3,nnprim))

    allprim = 0.0d0
    alll = 0

    ! copy to correct sized array
    do i=1, nnprim
        allprim(i) = tmpprim(i)
        alllabels(i) = tmplabels(i)
        alll(i) =  tmpl(i)
        allat(i) = tmpat(i)
        allcoords(:,i) = tmpcoords(:,i)
        allatoml2(:,i) = tmpatoml2(:,i)
    end do

    if (indebug .eqv. .true.) then

        print '(A10, A10, A12, A10, 3A16)', "l", "label", "alpha exp", "at label", "x", "y", "z"
        
        do i=1, nnprim

            print '(I10, A10, E12.6, A10, 3E16.6)', alll(i), alllabels(i), allprim(i), allat(i), allcoords(:,i)

        end do

    end if

    if (indebug .eqv. .true.) then

        print '(A10, A10, 3A5)', "l", "label", "nx", "ny", "nz"

        do i=1, nnprim

            print '(I10, A10, 3I5)', alll(i), alllabels(i), allatoml2(:,i)

        end do

    end if


    deallocate(tmpprim)
    deallocate(tmplabels)
    deallocate(tmpl)
    deallocate(tmpat)
    deallocate(tmpcoords)

    print*, ""
    print*, ""

    wnnprim = nnprim

end subroutine

subroutine readallrefbasis(inbasisdir, inrefbasisfilename, indebug)

    use common_variables

    use orbital_variables

    implicit none

    integer                                     :: iatoms
    character(len=150), dimension(natoms)        :: inrefbasisfilename
    character(len=150)                           :: refbasis
    character(len=150)                           :: inbasisdir
    character(len=5)                            :: zval
    integer                                     :: i

    real*8, dimension(:), allocatable           :: tmpprim
    character(len=5), dimension(:), allocatable :: tmplabels
    integer, dimension(:), allocatable          :: tmpl
    character(len=5), dimension(:), allocatable :: tmpat

    logical                                     :: indebug

    real*8, dimension(:,:), allocatable         :: tmpcoords
    integer, dimension(:,:), allocatable        :: tmpatoml2

    ! tmp array to hold all primitives
    allocate(tmpprim(1000))
    allocate(tmplabels(1000))
    allocate(tmpl(1000))
    allocate(tmpat(1000))
    allocate(tmpcoords(3,1000))
    allocate(tmpatoml2(3,1000))

    tmpprim = 0.0d0
    tmpl = 0
    nnprim = 0
    tmpatoml2 = 0

    print*, "================================================================"
    print*, "Loading reference basis files"
    print*, "================================================================"

    do iatoms=1, natoms

        ! stitch together path of basis file
        write(zval, '(I5)') atnums(iatoms)
        !print*, char(atnums(iatoms))
        refbasis = trim(inbasisdir) // trim(adjustl(zval)) // "/" // trim(inrefbasisfilename(iatoms))
        print*, "reference basis for atom ", atlabel(iatoms), "is ", refbasis

        nprim = 0
        call readgaussianbasis(refbasis, indebug)
        refnprim = nprim
        print*, "refnprim = ", refnprim


        do i=1, refnprim
            tmpprim(i+refnnprim) = prims(i)
            tmplabels(i+refnnprim) = labels(i)
            tmpl(i+refnnprim) = atoml(i)

            tmpat(i+refnnprim) = atlabel(iatoms)
            tmpcoords(:,i+refnnprim) = coords(:,iatoms)
            tmpatoml2(:,i+refnnprim) = atoml2(:,i)
        end do

        refnnprim = refnnprim + refnprim

        deallocate(prims)
        deallocate(labels)
        deallocate(atoml)
        deallocate(atoml2)

        print*, "================================================================"

    end do

    allocate(refallprim(refnnprim))
    allocate(refalllabels(refnnprim))
    allocate(refalll(refnnprim))
    allocate(refallat(refnnprim))
    allocate(refallcoords(3,refnnprim))
    allocate(refallatoml2(3,refnnprim))

    refallprim = 0.0d0
    refalll = 0

    ! copy to correct sized array
    do i=1, refnnprim
        refallprim(i) = tmpprim(i)
        refalllabels(i) = tmplabels(i)
        refalll(i) =  tmpl(i)
        refallat(i) = tmpat(i)
        refallcoords(:,i) = tmpcoords(:,i)
        refallatoml2(:,i) = tmpatoml2(:,i)
    end do

    if (indebug .eqv. .true.) then
        print '(A10, A10, A12, A10, 3A16)', "l", "label", "alpha exp", "at label", "x", "y", "z"
        
        do i=1, refnnprim
            print '(I10, A10, E12.6, A10, 3E16.6)', refalll(i), refalllabels(i), refallprim(i), &
                & refallat(i), refallcoords(:,i)
        end do

    end if

    if (indebug .eqv. .true.) then
        print '(A10, A10, 3A5)', "l", "label", "nx", "ny", "nz"

        do i=1, refnnprim
            print '(I10, A10, 3I5)', refalll(i), refalllabels(i), refallatoml2(:,i)
        end do
    end if


    deallocate(tmpprim)
    deallocate(tmplabels)
    deallocate(tmpl)
    deallocate(tmpat)
    deallocate(tmpcoords)

    print*, ""
    print*, ""


end subroutine

subroutine readgaussianbasis(inbasis, indebug)

    use common_variables

    use orbital_variables

    implicit none

    ! input parameters
    character(len=150)                   :: inbasis
    character(len=1)                    :: baslabel
    integer                             :: maxl
    logical                             :: indebug

    integer                             :: i, j
    integer                             :: idx, jdx, kdx
    integer                             :: nsprim, npprim, ndprim, nfprim, ngprim, nhprim

    real*8, dimension(:), allocatable   :: exps

    character(len=5), dimension(1)      :: slabels
    character(len=5), dimension(3)      :: plabels
    character(len=5), dimension(6)      :: dlabels
    character(len=5), dimension(10)     :: flabels
    character(len=5), dimension(1)      :: glabels
    character(len=5), dimension(1)      :: hlabels

    slabels = (/"s    "/)
    plabels = (/"px   ","py   ","pz   "/)
    dlabels = (/"dxx  ","dyy  ","dzz  ","dxy  ","dxz  ","dyz  "/)
    flabels = (/"fxxx ","fyyy ","fzzz ","fxxy ","fxxz ","fyyx ",&
               &"fyyz ","fzzx ","fzzy ","fxyz "/)
    glabels = (/"g500 "/)
    hlabels = (/"h600 "/)

    ! initial values of number of exponents
    nsprim = 0
    npprim = 0
    ndprim = 0
    nfprim = 0
    ngprim = 0
    nhprim = 0

    !allocate(exps(innexp))
    allocate(exps(10000))

    open(unit=10, file=trim(inbasis))

    ! read 1 useless line with the atom symbol
    read(10, *)

    idx = 0
    maxl = 0
    nsprim = 0
    npprim = 0
    ndprim = 0
    nfprim = 0
    ngprim = 0
    nhprim = 0
    
    !
    ! read possible basis label (S,P,D,...) until a * is read, then we are finshed
    !
10  read(10, '(1X,A1)', end=20, err=20) baslabel
    if (baslabel == 'S') then
        idx = idx + 1
        nsprim = nsprim + 1
        read(10, *, end=20, err=20) exps(idx)
    else if (baslabel == 'P') then
        idx = idx + 1
        npprim = npprim + 1
        read(10, *, end=20, err=20) exps(idx)
    else if (baslabel == 'D') then
        idx = idx + 1
        ndprim = ndprim + 1
        read(10, *, end=20, err=20) exps(idx)
    else if (baslabel == 'F') then
        idx = idx + 1
        nfprim = nfprim + 1
        read(10, *, end=20, err=20) exps(idx)
    else if (baslabel == 'G') then
        idx = idx + 1
        ngprim = ngprim + 1
        read(10, *, end=20, err=20) exps(idx)
    else if (baslabel == 'H') then
        idx = idx + 1
        nhprim = nhprim + 1
        read(10, *, end=20, err=20) exps(idx)
    endif 
    goto 10

20 close(unit=10)

    if (npprim > 0) maxl = maxl + 1
    if (ndprim > 0) maxl = maxl + 1
    if (nfprim > 0) maxl = maxl + 1
    if (ngprim > 0) maxl = maxl + 1
    if (nhprim > 0) maxl = maxl + 1

    print*, "max angular momentum = ", maxl

    !
    ! now copy exponents to match labels for each component
    !
    nprim = nsprim + 3*npprim + 6*ndprim + 10*nfprim + 15*ngprim + 21*nhprim 

    allocate(prims(nprim))
    allocate(labels(nprim))
    allocate(atoml(nprim))
    allocate(atoml2(3,nprim))

    ! reset counter
    idx = 0
    jdx = 0

    kdx = 0

    ! s functions
    if (maxl .ge. 0) then

        call makel(0)

        do i=1, nsprim
            prims(i) = exps(i)
            labels(i) = slabels(1)
            atoml(i) = 0
            atoml2(:,i) = cartl(:,1)
        end do

        deallocate(cartl)

    end if

    idx = idx + nsprim
    jdx = jdx + nsprim

    ! p functions
    if (maxl .ge. 1) then
        call makel(1)
        do i=1, npprim
            do j=1, 3
                kdx = kdx+1
                !print*, jdx
                prims(jdx + kdx ) = exps(idx+i)
                labels(jdx + kdx) = plabels(j)
                atoml(jdx + kdx) = 1
                atoml2(:,jdx+kdx) = cartl(:,j)
            end do    
        end do
        deallocate(cartl)
    end if

    jdx = jdx + 3*npprim
    idx = idx + npprim

    kdx = 0

    ! d functions
    if (maxl .ge. 2) then
        call makel(2)
        do i=1, ndprim
            do j=1, 6
                kdx = kdx+1
                prims(jdx+kdx) = exps(idx+i)
                labels(jdx+kdx) = dlabels(j)
                atoml(jdx + kdx) = 2
                atoml2(:,jdx+kdx) = cartl(:,j)
            end do    
        end do
        deallocate(cartl)
    end if

    jdx = jdx + 6*ndprim
    idx = idx + ndprim
    kdx = 0

    ! f functions
    if (maxl .ge. 3) then
        call makel(3)
        do i=1, nfprim
            do j=1, 10
                kdx = kdx+1
                prims(jdx+kdx) = exps(idx+i)
                labels(jdx+kdx) = flabels(j)
                atoml(jdx + kdx) = 3
                atoml2(:,jdx+kdx) = cartl(:,j)
            end do    
        end do
        deallocate(cartl)
    end if

    jdx = jdx + 10*nfprim
    idx = idx + nfprim
    kdx = 0

    ! g functions
    if (maxl .ge. 4) then
        call makel(4)

        do i=1, 15

            do j=1, ngprim

                kdx = kdx+1
                prims(jdx+kdx) = exps(idx+j)
                labels(jdx+kdx) = glabels(1)
                atoml(jdx + kdx) = 4
                atoml2(:,jdx+kdx) = cartl(:,i)

            end do    

        end do

        deallocate(cartl)

    end if

    jdx = jdx + 15*ngprim
    idx = idx + ngprim
    kdx = 0

    ! h functions
    if (maxl .ge. 5) then

        call makel(5)

        do i=1, 21

            do j=1, nhprim

                kdx = kdx+1
            
                prims(jdx+kdx) = exps(idx+j)
                labels(jdx+kdx) = hlabels(1)
                atoml(jdx + kdx) = 5
                atoml2(:,jdx+kdx) = cartl(:,i)
                
            end do    

        end do

        deallocate(cartl)

    end if

    if (indebug .eqv. .true.) then

        do i=1, nprim

            print '(A5, A5, E16.10, 3I5)', atoml(i), labels(i), prims(i), atoml2(:,i)

        end do

    end if

    deallocate(exps)

end subroutine

