#!/bin/bash
#set -x
################################################################################
#                                                                              #
# Shell-script for optimization of molecular orbitals from single-determinant  #
# to match reference, usually FCI                                              #
#  4/2022 PHJ                                                                  #
################################################################################
#
paramlist="$*"
#
#default values for options
BIN=/home/philip/speciale/program
export BIN
NCORES=0
NODES=1
MEM=0
TIME=1:00:00
QUEUE=q16
RESTARTSWEEP=0
REFDENS=0
FRJ_OPT=0
optfat=0
# 
# 
usage (){
          echo
          echo "  Usage:  phjiter [-np nodes] [-nc cores] [-t time] [-q queue] xxx"
          echo
          echo "  The input  files are assumed to have the name xxx.mol and xxx.dal"
          echo "  The output file is returned as xxx.out in the current directory"
          echo "  Modified for SLURM 10/2018"
          echo '  Options:'
          echo '  -q queue   (q12,q12fat,q16,q16l,q20,q20,q28,q36,qexp)        [default = q16]'
          echo '  -np nodes  number of nodes                  [default = 1]'
	  echo '  -t time    timelimit in cpu hours           [default = 1 hour]'
          echo '  -m         memory in gb                     [default = 48, 60, 60, 128, 256, 384 gb for the above queues]'
          echo '  -fat       submit to the 'fat' q12 and q20 nodes [default = no, if yes, then mem = 96 and 256]'
          echo '  -nc        number of cores on each node     [default = all cores]'
          echo '  -f         copy xxx.tar.gz file for restart or moread     '
          echo
         }

while [ -n "`echo "$1" | grep '-'`" ]; do
    case $1 in
      -fs ) RESTARTSWEEP=1;;
      -fd ) REFDENS=1;;
      -fo ) FRJ_OPT=1;;
      -fat ) optfat=1;;
      -q ) QUEUE=$2
           shift;;
      -t ) TIME="$2":00:00
           shift;;
      -m ) MEM=$2
           shift;;
      * ) usage; exit 1;;
   esac
   shift
done


#done
# 
# check for correct input
#if [ -z "$1" ]; then
##  echo 'Input file not specified'
   #usage
   #exit 1
#fi
#if [ ! -e $1.mol ] ; then
  #echo "MOL file not found -- aborting" 
  #exit 1
#else
  #MOL=$1
#fi
#if [ ! -e $1.dal ] ; then
  #echo "DAL file not found -- aborting" 
  #exit 1
#else
  #DAL=$1
#fi
##
#if [ $RESTART = 1 ] ; then
  #if [ ! -e $1.tar.gz ] ; then
    #echo "RESTART requested, but no $1.tar.gz file found -- aborting" 
    #exit 1
  #fi
#fi
#


#
#  check for OUT file
if [ -e $1.out ] ; then
  echo "Moving OUT file to backup"
  mv $1.out $1.out.back
fi

#  check for REFDENS file
if [[ -e $1.REFDENS && $REFDENS == 0 ]] ; then
  echo "Moving REFDENS file to backup"
  mv $1.REFDENS $1.REFDENS.back
fi
#  check for converged alphas fort.21 file
if [[ -e $1.fort.21 && $RESTARTSWEEP == 0 ]] ; then
  echo "Moving converged alphas fort.21 file to backup"
  mv $1.fort.21 $1.fort.21.back
fi

#

if [ $QUEUE = q8 ] ; then
     NCORES=8
fi
if [ $QUEUE = q12 ] ; then
     NCORES=12
fi
if [ $QUEUE = q12fat ] ; then
     NCORES=12
fi
if [ $QUEUE = q16 ] ; then
     NCORES=16
fi
if [ $QUEUE = q16l ] ; then
     NCORES=16
fi
if [ $QUEUE = q20 ] ; then
     NCORES=20
fi
if [ $QUEUE = q24 ] ; then
     NCORES=24
fi
if [ $QUEUE = q28 ] ; then
     NCORES=28
fi
if [ $QUEUE = qtest ] ; then
     NCORES=24
fi
if [ $QUEUE = q36 ] ; then
     NCORES=36
fi
if [ $QUEUE = qfat ] ; then
     NCORES=40
fi
if [ $QUEUE = q40 ] ; then
     NCORES=40
fi


# total number of cpus

NCPUS=$(($NCORES * $NODES))

# create submit script
#
echo "#!/bin/bash"                 > runphjiter.$1
echo "#SBATCH --job-name="$1"  "   >> runphjiter.$1 
echo "#SBATCH --nodes="$NODES" "   >> runphjiter.$1 
echo "#SBATCH --time="$TIME"     " >> runphjiter.$1 
echo "#SBATCH --exclusive"         >> runphjiter.$1 
echo "#SBATCH --partition=$QUEUE"  >> runphjiter.$1 
echo "#SBATCH --mem=0"             >> runphjiter.$1

# load stuff

echo "module load gcc"           >> runphjiter.$1

echo "export OMP_NUM_THREADS=$NCORES" >> runphjiter.$1

echo "pwd" >> runphjiter.$1
echo "echo "rotmo.x compiled at:"" >> runphjiter.$1
echo "ls -lth /home/philip/speciale/program/rotmo.x" >> runphjiter.$1


# cp main executable
echo "cp $BIN/rotmo.x /scratch/"'$SLURM_JOB_ID'"/rotmo.x" >> runphjiter.$1

# cp frj opt
#echo "cp $BIN/NR_optim_hess/genstp4.x /scratch/"'$SLURM_JOB_ID'"/genstp4.x" >> runphjiter.$1
echo "cp $BIN/NR_optim_hess/genstp5.x /scratch/"'$SLURM_JOB_ID'"/genstp5.x" >> runphjiter.$1


# cp input file
echo "cp  "$1".inp /scratch/"'$SLURM_JOB_ID'"/"$1".inp" >> runphjiter.$1

# if load REFDENS, cp $1.REFDENS
if [ $REFDENS = 1 ]; then
    echo "Restart of REFDENS requested, copying $1.REFDENS to REFDENS"
    echo "cp "$1".REFDENS /scratch/"'$SLURM_JOB_ID'"/REFDENS" >> runphjiter.$1
fi

# if restart sweep, cp $1.fort.21
if [ $RESTARTSWEEP = 1 ]; then
    echo "Restart of alphas requested, copying $1.fort.21 to fort.90"
    echo "cp "$1".fort.21 /scratch/"'$SLURM_JOB_ID'"/fort.90" >> runphjiter.$1
fi

echo "FRJ_OPT = TRUE, copying fort.13 to scratch"
echo "cp fort.13 /scratch/"'$SLURM_JOB_ID'"/fort.13" >> runphjiter.$1

# also cp popt.lst
echo "cp popt.lst /scratch/"'$SLURM_JOB_ID'"/popt.lst" >> runphjiter.$1




echo "cd /scratch/"'$SLURM_JOB_ID'" " >> runphjiter.$1

#####
# now starting actual optimization
####


#echo "maxiter=500                                   " >> runphjiter.$1
echo "for i in {1..$2}; do                     " >> runphjiter.$1
#echo "      echo "'opt iter = '$i''"                        " >> runphjiter.$1
echo "echo "'"iter $i "'" >> opt.log                         " >> runphjiter.$1
echo "echo "'" "'" >> opt.log                         " >> runphjiter.$1
echo "echo "'" "'" >> opt.log                         " >> runphjiter.$1
echo "date >> opt.log                             " >> runphjiter.$1
echo "cp fort.13 fort.10                          " >> runphjiter.$1
      # takes fort.10, makes fort.9 and fort.12
echo "./rotmo.x < "$1".inp > rotmo.out."'$i'"           " >> runphjiter.$1
#echo "ls -lthrs " >> runphjiter.$1
echo "./genstp5.x >> opt.log " >> runphjiter.$1
      # stopping criteria, per STOP file from opt
echo "if [ -f "STOP" ]; then                      " >> runphjiter.$1
echo "  echo "Found STOP, breaking"                                    " >> runphjiter.$1
echo "  break                                    " >> runphjiter.$1
echo "fi                                          " >> runphjiter.$1
echo "done                                          " >> runphjiter.$1



# cp back opt.log
echo "cp opt.log "'$SLURM_SUBMIT_DIR'"/opt.log " >> runphjiter.$1

# cp back final alphas
echo "cp fort.13 "'$SLURM_SUBMIT_DIR'"/fort.23 " >> runphjiter.$1

echo "ls -lthrs " >> runphjiter.$1

# cp back archive
echo "echo "Making archive of files" " >> runphjiter.$1
echo "tar -czf "$1".tar.gz ." >> runphjiter.$1
echo "cp "$1".tar.gz "'$SLURM_SUBMIT_DIR'"/"$1".tar.gz " >> runphjiter.$1 



# test for now
#echo "cp fort.99 "'$SLURM_SUBMIT_DIR'"/"$1".fort.99 " >> runphjiter.$1

# cp back refdens only if not loaded from external file
#if [ $REFDENS = 0 ]; then
    #echo "cp fort.19 "'$SLURM_SUBMIT_DIR'"/"$1".REFDENS " >> runphjiter.$1
#fi 

#echo "tree" >> runphjiter.$1

# 
chmod +x runphjiter.$1


sbatch runphjiter.$1
