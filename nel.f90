!==============================================================================
!
!    This file is part of ROTMO.
!
!    ROTMO is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    ROTMO is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with ROTMO. If not, see http://www.gnu.org/licenses.
!
!==============================================================================
!
! obviously assumes the same number of points as the grid
! from collectgrid subroutine
!
!==========================================================

subroutine sumdens(indata_array, inprint)
    
    use grid_variables

    use orbital_variables

    use common_variables

    implicit none

    logical                         :: inprint

    ! input parameters
    real*8, dimension(npoints)      :: indata_array

    if (inprint .eqv. .true. ) then

        print*, "================================================================"
        print*, "Sum over all densities to get number of electrons"
        print*, "number of grid points", npoints

    end if

    nelectrons = sum(indata_array*grid(4,:)) 

    if (inprint .eqv. .true. ) then

        print '(A25, f24.14)', "nelectrons = ", nelectrons
        print*, ""

    end if

    ! check for deviation in number of electrons
    if (abs(sum(dble(atnums)) - nelectrons) .gt. 1.0d+8 ) then
!FRJ    if (abs(sum(dble(atnums)) - nelectrons) .gt. 1.0d-8 ) then
        
        print*, "abs(sum(dble(atnums)) - nelectrons) = ", abs(sum(dble(atnums)) - nelectrons)
        error stop "Number of electrons is deviating too much, tolerance is 1.0d-8"

    end if

end subroutine sumdens

